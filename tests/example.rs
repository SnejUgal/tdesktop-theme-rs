extern crate tdesktop_theme;

use std::fs;
use tdesktop_theme::{TdesktopTheme, Value, WallpaperType};

#[test]
fn main() {
  let contents = fs::read("./tests/assets/cortana.tdesktop-theme").unwrap();
  let mut theme = TdesktopTheme::from_bytes(&contents[..]).unwrap();

  if let Some(wallpaper) = &theme.wallpaper {
    print!("The theme has a wallpaper and it's ");

    match wallpaper.wallpaper_type {
      WallpaperType::Tiled => println!("tiled"),
      WallpaperType::Background => println!("covering the screen"),
    };

    let extension = wallpaper.extension.to_string();

    fs::write(
      format!("./tests/ignore/wallpaper.{}", extension),
      &wallpaper.bytes,
    )
    .unwrap();
  }

  theme.set_variable("windowBg".to_string(), [0xff; 4]).unwrap();
  theme
    .link_variable("windowBoldFg".to_string(), "windowFg".to_string())
    .unwrap();

  match theme.get_variable("windowBoldFg").unwrap() {
    Value::Color(_) => println!("windowBoldFg is not linked"),
    Value::Link(link) => println!("windowBoldFg is linked to {}", link),
  }

  println!("{:?}", theme.resolve_variable("windowBoldFg").unwrap());

  theme.delete_variable("windowActiveBg");
  theme.unlink_variable("windowBoldFg");
}
