# `tdesktop-theme-rs`

A Rust crate for parsing and serialization of `.tdesktop-theme` and
`.tdesktop-palette` files.

## Installing

Add this in your `Cargo.toml`:

```toml
[dependencies]
tdesktop_theme = "0.3"
```

## Examples

See the [`tests`] directory for examples.

## Documentation

See [_docs.rs_][docs] for documentation.

[`tests`]: ./tests/
[docs]: https://docs.rs/tdesktop_theme/
