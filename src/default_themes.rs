//! Provides Telegram Desktop's default themes.
//!
//! As of 1.3.17, Telegram has four default themes. Here are their names and
//! functions that generate them.
//!
//! Name     | Function
//! -------- | ----------------------------
//! Blue     | `default_themes::blue()`
//! Classic  | `default_themes::classic()`
//! Midnight | `default_themes::midnight()`
//! Matrix   | `default_themes::matrix()`
//!
//! # Notes
//!
//! If you want to fallback to the default theme, use
//! `default_themes::classic()`. Note that it's absolutely okay that the default
//! theme links to the original theme's variables.
//!
//! The themes are generated on each call. You should generate such themes once
//! and store them somewhere. When using them to fallback, use should use
//! `&theme | &default_theme` instead of `theme | default_theme` and clone them
//! when using `|=`.

use super::*;

mod blue;
mod classic;
mod matrix;
mod midnight;

pub use self::blue::*;
pub use self::classic::*;
pub use self::matrix::*;
pub use self::midnight::*;
