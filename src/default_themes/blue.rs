use super::*;

/// Generates the default Blue theme theme.
#[must_use]
pub fn blue() -> TdesktopTheme {
  let mut theme = TdesktopTheme::with_capacity(358);

  theme.set_variable("windowBg".to_string(), [0xff, 0xff, 0xff, 0xff]).unwrap();
  theme.set_variable("windowFg".to_string(), [0x00, 0x00, 0x00, 0xff]).unwrap();
  theme
    .set_variable("windowBgOver".to_string(), [0xf1, 0xf1, 0xf1, 0xff])
    .unwrap();
  theme
    .set_variable("windowBgRipple".to_string(), [0xe5, 0xe5, 0xe5, 0xff])
    .unwrap();
  theme
    .link_variable("windowFgOver".to_string(), "windowFg".to_string())
    .unwrap();
  theme
    .set_variable("windowSubTextFg".to_string(), [0x99, 0x99, 0x99, 0xff])
    .unwrap();
  theme
    .set_variable("windowSubTextFgOver".to_string(), [0x91, 0x91, 0x91, 0xff])
    .unwrap();
  theme
    .set_variable("windowBoldFg".to_string(), [0x22, 0x22, 0x22, 0xff])
    .unwrap();
  theme
    .set_variable("windowBoldFgOver".to_string(), [0x22, 0x22, 0x22, 0xff])
    .unwrap();
  theme
    .set_variable("windowBgActive".to_string(), [0x40, 0xa7, 0xe3, 0xff])
    .unwrap();
  theme
    .set_variable("windowFgActive".to_string(), [0xff, 0xff, 0xff, 0xff])
    .unwrap();
  theme
    .set_variable("windowActiveTextFg".to_string(), [0x16, 0x8a, 0xcd, 0xff])
    .unwrap();
  theme
    .set_variable("windowShadowFg".to_string(), [0x00, 0x00, 0x00, 0xff])
    .unwrap();
  theme
    .set_variable(
      "windowShadowFgFallback".to_string(),
      [0xf1, 0xf1, 0xf1, 0xff],
    )
    .unwrap();
  theme.set_variable("shadowFg".to_string(), [0x00, 0x00, 0x00, 0x18]).unwrap();
  theme
    .set_variable("slideFadeOutBg".to_string(), [0x00, 0x00, 0x00, 0x3c])
    .unwrap();
  theme
    .link_variable(
      "slideFadeOutShadowFg".to_string(),
      "windowShadowFg".to_string(),
    )
    .unwrap();
  theme.set_variable("imageBg".to_string(), [0x00, 0x00, 0x00, 0xff]).unwrap();
  theme
    .set_variable("imageBgTransparent".to_string(), [0xff, 0xff, 0xff, 0xff])
    .unwrap();
  theme
    .link_variable("activeButtonBg".to_string(), "windowBgActive".to_string())
    .unwrap();
  theme
    .set_variable("activeButtonBgOver".to_string(), [0x39, 0xa5, 0xdb, 0xff])
    .unwrap();
  theme
    .set_variable("activeButtonBgRipple".to_string(), [0x20, 0x95, 0xd0, 0xff])
    .unwrap();
  theme
    .link_variable("activeButtonFg".to_string(), "windowFgActive".to_string())
    .unwrap();
  theme
    .link_variable(
      "activeButtonFgOver".to_string(),
      "activeButtonFg".to_string(),
    )
    .unwrap();
  theme
    .set_variable(
      "activeButtonSecondaryFg".to_string(),
      [0xcc, 0xee, 0xff, 0xff],
    )
    .unwrap();
  theme
    .link_variable(
      "activeButtonSecondaryFgOver".to_string(),
      "activeButtonSecondaryFg".to_string(),
    )
    .unwrap();
  theme
    .set_variable("activeLineFg".to_string(), [0x37, 0xa1, 0xde, 0xff])
    .unwrap();
  theme
    .set_variable("activeLineFgError".to_string(), [0xe4, 0x83, 0x83, 0xff])
    .unwrap();
  theme
    .link_variable("lightButtonBg".to_string(), "windowBg".to_string())
    .unwrap();
  theme
    .set_variable("lightButtonBgOver".to_string(), [0xe3, 0xf1, 0xfa, 0xff])
    .unwrap();
  theme
    .set_variable("lightButtonBgRipple".to_string(), [0xc9, 0xe4, 0xf6, 0xff])
    .unwrap();
  theme
    .link_variable(
      "lightButtonFg".to_string(),
      "windowActiveTextFg".to_string(),
    )
    .unwrap();
  theme
    .link_variable("lightButtonFgOver".to_string(), "lightButtonFg".to_string())
    .unwrap();
  theme
    .set_variable("attentionButtonFg".to_string(), [0xd1, 0x4e, 0x4e, 0xff])
    .unwrap();
  theme
    .set_variable("attentionButtonFgOver".to_string(), [0xd1, 0x4e, 0x4e, 0xff])
    .unwrap();
  theme
    .set_variable("attentionButtonBgOver".to_string(), [0xfc, 0xdf, 0xde, 0xff])
    .unwrap();
  theme
    .set_variable(
      "attentionButtonBgRipple".to_string(),
      [0xf4, 0xc3, 0xc2, 0xff],
    )
    .unwrap();
  theme
    .link_variable("outlineButtonBg".to_string(), "windowBg".to_string())
    .unwrap();
  theme
    .link_variable(
      "outlineButtonBgOver".to_string(),
      "lightButtonBgOver".to_string(),
    )
    .unwrap();
  theme
    .link_variable(
      "outlineButtonOutlineFg".to_string(),
      "windowBgActive".to_string(),
    )
    .unwrap();
  theme
    .link_variable(
      "outlineButtonBgRipple".to_string(),
      "lightButtonBgRipple".to_string(),
    )
    .unwrap();
  theme.link_variable("menuBg".to_string(), "windowBg".to_string()).unwrap();
  theme
    .link_variable("menuBgOver".to_string(), "windowBgOver".to_string())
    .unwrap();
  theme
    .link_variable("menuBgRipple".to_string(), "windowBgRipple".to_string())
    .unwrap();
  theme
    .set_variable("menuIconFg".to_string(), [0xa8, 0xa8, 0xa8, 0xff])
    .unwrap();
  theme
    .set_variable("menuIconFgOver".to_string(), [0x99, 0x99, 0x99, 0xff])
    .unwrap();
  theme
    .set_variable("menuSubmenuArrowFg".to_string(), [0x37, 0x37, 0x37, 0xff])
    .unwrap();
  theme
    .set_variable("menuFgDisabled".to_string(), [0xcc, 0xcc, 0xcc, 0xff])
    .unwrap();
  theme
    .set_variable("menuSeparatorFg".to_string(), [0xf1, 0xf1, 0xf1, 0xff])
    .unwrap();
  theme
    .set_variable("scrollBarBg".to_string(), [0x00, 0x00, 0x00, 0x53])
    .unwrap();
  theme
    .set_variable("scrollBarBgOver".to_string(), [0x00, 0x00, 0x00, 0x7a])
    .unwrap();
  theme.set_variable("scrollBg".to_string(), [0x00, 0x00, 0x00, 0x00]).unwrap();
  theme
    .set_variable("scrollBgOver".to_string(), [0x00, 0x00, 0x00, 0x1a])
    .unwrap();
  theme
    .set_variable("smallCloseIconFg".to_string(), [0xc7, 0xc7, 0xc7, 0xff])
    .unwrap();
  theme
    .set_variable("smallCloseIconFgOver".to_string(), [0xa3, 0xa3, 0xa3, 0xff])
    .unwrap();
  theme
    .link_variable("radialFg".to_string(), "windowFgActive".to_string())
    .unwrap();
  theme.set_variable("radialBg".to_string(), [0x00, 0x00, 0x00, 0x56]).unwrap();
  theme
    .link_variable("placeholderFg".to_string(), "windowSubTextFg".to_string())
    .unwrap();
  theme
    .set_variable("placeholderFgActive".to_string(), [0xaa, 0xaa, 0xaa, 0xff])
    .unwrap();
  theme
    .set_variable("inputBorderFg".to_string(), [0xe0, 0xe0, 0xe0, 0xff])
    .unwrap();
  theme
    .set_variable("filterInputBorderFg".to_string(), [0x54, 0xc3, 0xf3, 0xff])
    .unwrap();
  theme
    .set_variable("checkboxFg".to_string(), [0xb3, 0xb3, 0xb3, 0xff])
    .unwrap();
  theme
    .set_variable("sliderBgInactive".to_string(), [0xe1, 0xea, 0xef, 0xff])
    .unwrap();
  theme
    .link_variable("sliderBgActive".to_string(), "windowBgActive".to_string())
    .unwrap();
  theme
    .set_variable("tooltipBg".to_string(), [0xee, 0xf2, 0xf5, 0xff])
    .unwrap();
  theme
    .set_variable("tooltipFg".to_string(), [0x5d, 0x6c, 0x80, 0xff])
    .unwrap();
  theme
    .set_variable("tooltipBorderFg".to_string(), [0xc9, 0xd1, 0xdb, 0xff])
    .unwrap();
  theme
    .link_variable("titleBg".to_string(), "windowBgOver".to_string())
    .unwrap();
  theme
    .set_variable("titleShadow".to_string(), [0x00, 0x00, 0x00, 0x03])
    .unwrap();
  theme
    .set_variable("titleButtonFg".to_string(), [0xab, 0xab, 0xab, 0xff])
    .unwrap();
  theme
    .set_variable("titleButtonBgOver".to_string(), [0xe5, 0xe5, 0xe5, 0xff])
    .unwrap();
  theme
    .set_variable("titleButtonFgOver".to_string(), [0x9a, 0x9a, 0x9a, 0xff])
    .unwrap();
  theme
    .set_variable(
      "titleButtonCloseBgOver".to_string(),
      [0xe8, 0x11, 0x23, 0xff],
    )
    .unwrap();
  theme
    .link_variable(
      "titleButtonCloseFgOver".to_string(),
      "windowFgActive".to_string(),
    )
    .unwrap();
  theme
    .set_variable("titleFgActive".to_string(), [0x3e, 0x3c, 0x3e, 0xff])
    .unwrap();
  theme.set_variable("titleFg".to_string(), [0xac, 0xac, 0xac, 0xff]).unwrap();
  theme
    .set_variable("trayCounterBg".to_string(), [0xf2, 0x3c, 0x34, 0xff])
    .unwrap();
  theme
    .set_variable("trayCounterBgMute".to_string(), [0x88, 0x88, 0x88, 0xff])
    .unwrap();
  theme
    .set_variable("trayCounterFg".to_string(), [0xff, 0xff, 0xff, 0xff])
    .unwrap();
  theme
    .set_variable(
      "trayCounterBgMacInvert".to_string(),
      [0xff, 0xff, 0xff, 0xff],
    )
    .unwrap();
  theme
    .set_variable(
      "trayCounterFgMacInvert".to_string(),
      [0xff, 0xff, 0xff, 0x01],
    )
    .unwrap();
  theme.set_variable("layerBg".to_string(), [0x00, 0x00, 0x00, 0x7f]).unwrap();
  theme
    .link_variable("cancelIconFg".to_string(), "menuIconFg".to_string())
    .unwrap();
  theme
    .link_variable("cancelIconFgOver".to_string(), "menuIconFgOver".to_string())
    .unwrap();
  theme.link_variable("boxBg".to_string(), "windowBg".to_string()).unwrap();
  theme.link_variable("boxTextFg".to_string(), "windowFg".to_string()).unwrap();
  theme
    .set_variable("boxTextFgGood".to_string(), [0x4a, 0xb4, 0x4a, 0xff])
    .unwrap();
  theme
    .set_variable("boxTextFgError".to_string(), [0xd8, 0x4d, 0x4d, 0xff])
    .unwrap();
  theme
    .set_variable("boxTitleFg".to_string(), [0x40, 0x40, 0x40, 0xff])
    .unwrap();
  theme.link_variable("boxSearchBg".to_string(), "boxBg".to_string()).unwrap();
  theme
    .link_variable(
      "boxSearchCancelIconFg".to_string(),
      "cancelIconFg".to_string(),
    )
    .unwrap();
  theme
    .link_variable(
      "boxSearchCancelIconFgOver".to_string(),
      "cancelIconFgOver".to_string(),
    )
    .unwrap();
  theme
    .set_variable("boxTitleAdditionalFg".to_string(), [0x80, 0x80, 0x80, 0xff])
    .unwrap();
  theme
    .link_variable("boxTitleCloseFg".to_string(), "cancelIconFg".to_string())
    .unwrap();
  theme
    .link_variable(
      "boxTitleCloseFgOver".to_string(),
      "cancelIconFgOver".to_string(),
    )
    .unwrap();
  theme
    .link_variable(
      "membersAboutLimitFg".to_string(),
      "windowSubTextFgOver".to_string(),
    )
    .unwrap();
  theme
    .link_variable("contactsBg".to_string(), "windowBg".to_string())
    .unwrap();
  theme
    .link_variable("contactsBgOver".to_string(), "windowBgOver".to_string())
    .unwrap();
  theme
    .link_variable("contactsNameFg".to_string(), "boxTextFg".to_string())
    .unwrap();
  theme
    .link_variable(
      "contactsStatusFg".to_string(),
      "windowSubTextFg".to_string(),
    )
    .unwrap();
  theme
    .link_variable(
      "contactsStatusFgOver".to_string(),
      "windowSubTextFgOver".to_string(),
    )
    .unwrap();
  theme
    .link_variable(
      "contactsStatusFgOnline".to_string(),
      "windowActiveTextFg".to_string(),
    )
    .unwrap();
  theme
    .link_variable("photoCropFadeBg".to_string(), "layerBg".to_string())
    .unwrap();
  theme
    .set_variable("photoCropPointFg".to_string(), [0xff, 0xff, 0xff, 0x7f])
    .unwrap();
  theme.link_variable("introBg".to_string(), "windowBg".to_string()).unwrap();
  theme
    .link_variable("introTitleFg".to_string(), "windowBoldFg".to_string())
    .unwrap();
  theme
    .link_variable(
      "introDescriptionFg".to_string(),
      "windowSubTextFg".to_string(),
    )
    .unwrap();
  theme
    .link_variable("introErrorFg".to_string(), "windowSubTextFg".to_string())
    .unwrap();
  theme
    .set_variable("introCoverTopBg".to_string(), [0x0f, 0x89, 0xd0, 0xff])
    .unwrap();
  theme
    .set_variable("introCoverBottomBg".to_string(), [0x39, 0xb0, 0xf0, 0xff])
    .unwrap();
  theme
    .set_variable("introCoverIconsFg".to_string(), [0x5e, 0xc6, 0xff, 0xff])
    .unwrap();
  theme
    .set_variable("introCoverPlaneTrace".to_string(), [0x5e, 0xc6, 0xff, 0x69])
    .unwrap();
  theme
    .set_variable("introCoverPlaneInner".to_string(), [0xc6, 0xd8, 0xe8, 0xff])
    .unwrap();
  theme
    .set_variable("introCoverPlaneOuter".to_string(), [0xa1, 0xbe, 0xd4, 0xff])
    .unwrap();
  theme
    .set_variable("introCoverPlaneTop".to_string(), [0xff, 0xff, 0xff, 0xff])
    .unwrap();
  theme
    .link_variable("dialogsMenuIconFg".to_string(), "menuIconFg".to_string())
    .unwrap();
  theme
    .link_variable(
      "dialogsMenuIconFgOver".to_string(),
      "menuIconFgOver".to_string(),
    )
    .unwrap();
  theme.link_variable("dialogsBg".to_string(), "windowBg".to_string()).unwrap();
  theme
    .link_variable("dialogsNameFg".to_string(), "windowBoldFg".to_string())
    .unwrap();
  theme
    .link_variable("dialogsChatIconFg".to_string(), "dialogsNameFg".to_string())
    .unwrap();
  theme
    .link_variable("dialogsDateFg".to_string(), "windowSubTextFg".to_string())
    .unwrap();
  theme
    .link_variable("dialogsTextFg".to_string(), "windowSubTextFg".to_string())
    .unwrap();
  theme
    .link_variable(
      "dialogsTextFgService".to_string(),
      "windowActiveTextFg".to_string(),
    )
    .unwrap();
  theme
    .set_variable("dialogsDraftFg".to_string(), [0xdd, 0x4b, 0x39, 0xff])
    .unwrap();
  theme
    .link_variable(
      "dialogsVerifiedIconBg".to_string(),
      "windowBgActive".to_string(),
    )
    .unwrap();
  theme
    .link_variable(
      "dialogsVerifiedIconFg".to_string(),
      "windowFgActive".to_string(),
    )
    .unwrap();
  theme
    .set_variable("dialogsSendingIconFg".to_string(), [0xc1, 0xc1, 0xc1, 0xff])
    .unwrap();
  theme
    .set_variable("dialogsSentIconFg".to_string(), [0x2c, 0xa6, 0xe8, 0xff])
    .unwrap();
  theme
    .link_variable("dialogsUnreadBg".to_string(), "windowBgActive".to_string())
    .unwrap();
  theme
    .set_variable("dialogsUnreadBgMuted".to_string(), [0xbb, 0xbb, 0xbb, 0xff])
    .unwrap();
  theme
    .link_variable("dialogsUnreadFg".to_string(), "windowFgActive".to_string())
    .unwrap();
  theme
    .link_variable("dialogsBgOver".to_string(), "windowBgOver".to_string())
    .unwrap();
  theme
    .link_variable(
      "dialogsNameFgOver".to_string(),
      "windowBoldFgOver".to_string(),
    )
    .unwrap();
  theme
    .link_variable(
      "dialogsChatIconFgOver".to_string(),
      "dialogsNameFgOver".to_string(),
    )
    .unwrap();
  theme
    .link_variable(
      "dialogsDateFgOver".to_string(),
      "windowSubTextFgOver".to_string(),
    )
    .unwrap();
  theme
    .link_variable(
      "dialogsTextFgOver".to_string(),
      "windowSubTextFgOver".to_string(),
    )
    .unwrap();
  theme
    .link_variable(
      "dialogsTextFgServiceOver".to_string(),
      "dialogsTextFgService".to_string(),
    )
    .unwrap();
  theme
    .link_variable(
      "dialogsDraftFgOver".to_string(),
      "dialogsDraftFg".to_string(),
    )
    .unwrap();
  theme
    .link_variable(
      "dialogsVerifiedIconBgOver".to_string(),
      "dialogsVerifiedIconBg".to_string(),
    )
    .unwrap();
  theme
    .link_variable(
      "dialogsVerifiedIconFgOver".to_string(),
      "dialogsVerifiedIconFg".to_string(),
    )
    .unwrap();
  theme
    .link_variable(
      "dialogsSendingIconFgOver".to_string(),
      "dialogsSendingIconFg".to_string(),
    )
    .unwrap();
  theme
    .link_variable(
      "dialogsSentIconFgOver".to_string(),
      "dialogsSentIconFg".to_string(),
    )
    .unwrap();
  theme
    .link_variable(
      "dialogsUnreadBgOver".to_string(),
      "dialogsUnreadBg".to_string(),
    )
    .unwrap();
  theme
    .link_variable(
      "dialogsUnreadBgMutedOver".to_string(),
      "dialogsUnreadBgMuted".to_string(),
    )
    .unwrap();
  theme
    .link_variable(
      "dialogsUnreadFgOver".to_string(),
      "dialogsUnreadFg".to_string(),
    )
    .unwrap();
  theme
    .set_variable("dialogsBgActive".to_string(), [0x41, 0x9f, 0xd9, 0xff])
    .unwrap();
  theme
    .link_variable(
      "dialogsNameFgActive".to_string(),
      "windowFgActive".to_string(),
    )
    .unwrap();
  theme
    .link_variable(
      "dialogsChatIconFgActive".to_string(),
      "dialogsNameFgActive".to_string(),
    )
    .unwrap();
  theme
    .link_variable(
      "dialogsDateFgActive".to_string(),
      "windowFgActive".to_string(),
    )
    .unwrap();
  theme
    .link_variable(
      "dialogsTextFgActive".to_string(),
      "windowFgActive".to_string(),
    )
    .unwrap();
  theme
    .link_variable(
      "dialogsTextFgServiceActive".to_string(),
      "dialogsTextFgActive".to_string(),
    )
    .unwrap();
  theme
    .set_variable("dialogsDraftFgActive".to_string(), [0xc6, 0xe1, 0xf7, 0xff])
    .unwrap();
  theme
    .link_variable(
      "dialogsVerifiedIconBgActive".to_string(),
      "dialogsTextFgActive".to_string(),
    )
    .unwrap();
  theme
    .link_variable(
      "dialogsVerifiedIconFgActive".to_string(),
      "dialogsBgActive".to_string(),
    )
    .unwrap();
  theme
    .set_variable(
      "dialogsSendingIconFgActive".to_string(),
      [0xff, 0xff, 0xff, 0x99],
    )
    .unwrap();
  theme
    .link_variable(
      "dialogsSentIconFgActive".to_string(),
      "dialogsTextFgActive".to_string(),
    )
    .unwrap();
  theme
    .link_variable(
      "dialogsUnreadBgActive".to_string(),
      "dialogsTextFgActive".to_string(),
    )
    .unwrap();
  theme
    .link_variable(
      "dialogsUnreadBgMutedActive".to_string(),
      "dialogsDraftFgActive".to_string(),
    )
    .unwrap();
  theme
    .link_variable(
      "dialogsUnreadFgActive".to_string(),
      "dialogsBgActive".to_string(),
    )
    .unwrap();
  theme
    .link_variable(
      "dialogsForwardBg".to_string(),
      "dialogsBgActive".to_string(),
    )
    .unwrap();
  theme
    .link_variable(
      "dialogsForwardFg".to_string(),
      "dialogsNameFgActive".to_string(),
    )
    .unwrap();
  theme
    .link_variable("searchedBarBg".to_string(), "windowBgOver".to_string())
    .unwrap();
  theme
    .link_variable("searchedBarBorder".to_string(), "shadowFg".to_string())
    .unwrap();
  theme
    .link_variable(
      "searchedBarFg".to_string(),
      "windowSubTextFgOver".to_string(),
    )
    .unwrap();
  theme.link_variable("topBarBg".to_string(), "windowBg".to_string()).unwrap();
  theme
    .link_variable("emojiPanBg".to_string(), "windowBg".to_string())
    .unwrap();
  theme
    .set_variable("emojiPanCategories".to_string(), [0xf7, 0xf7, 0xf7, 0xff])
    .unwrap();
  theme
    .link_variable(
      "emojiPanHeaderFg".to_string(),
      "windowSubTextFg".to_string(),
    )
    .unwrap();
  theme
    .set_variable("emojiPanHeaderBg".to_string(), [0xff, 0xff, 0xff, 0xf2])
    .unwrap();
  theme
    .set_variable("stickerPanDeleteBg".to_string(), [0x00, 0x00, 0x00, 0xcc])
    .unwrap();
  theme
    .link_variable(
      "stickerPanDeleteFg".to_string(),
      "windowFgActive".to_string(),
    )
    .unwrap();
  theme
    .set_variable("stickerPreviewBg".to_string(), [0xff, 0xff, 0xff, 0xb0])
    .unwrap();
  theme
    .link_variable("historyTextInFg".to_string(), "windowFg".to_string())
    .unwrap();
  theme
    .link_variable("historyTextOutFg".to_string(), "windowFg".to_string())
    .unwrap();
  theme
    .link_variable(
      "historyCaptionInFg".to_string(),
      "historyTextInFg".to_string(),
    )
    .unwrap();
  theme
    .link_variable(
      "historyCaptionOutFg".to_string(),
      "historyTextOutFg".to_string(),
    )
    .unwrap();
  theme
    .link_variable(
      "historyFileNameInFg".to_string(),
      "historyTextInFg".to_string(),
    )
    .unwrap();
  theme
    .link_variable(
      "historyFileNameOutFg".to_string(),
      "historyTextOutFg".to_string(),
    )
    .unwrap();
  theme
    .set_variable("historyOutIconFg".to_string(), [0x05, 0x9d, 0xe8, 0xff])
    .unwrap();
  theme
    .set_variable(
      "historyOutIconFgSelected".to_string(),
      [0x14, 0x9c, 0xe6, 0xff],
    )
    .unwrap();
  theme
    .link_variable(
      "historyIconFgInverted".to_string(),
      "windowFgActive".to_string(),
    )
    .unwrap();
  theme
    .set_variable(
      "historySendingOutIconFg".to_string(),
      [0x9d, 0xc2, 0xd9, 0xff],
    )
    .unwrap();
  theme
    .set_variable(
      "historySendingInIconFg".to_string(),
      [0xa0, 0xad, 0xb5, 0xff],
    )
    .unwrap();
  theme
    .set_variable(
      "historySendingInvertedIconFg".to_string(),
      [0xff, 0xff, 0xff, 0xc8],
    )
    .unwrap();
  theme
    .set_variable("historySystemBg".to_string(), [0x89, 0xa0, 0xb4, 0x7f])
    .unwrap();
  theme
    .set_variable(
      "historySystemBgSelected".to_string(),
      [0xbb, 0xc8, 0xd4, 0xa2],
    )
    .unwrap();
  theme
    .link_variable("historySystemFg".to_string(), "windowFgActive".to_string())
    .unwrap();
  theme
    .set_variable("historyUnreadBarBg".to_string(), [0xfc, 0xfb, 0xfa, 0xff])
    .unwrap();
  theme
    .link_variable("historyUnreadBarBorder".to_string(), "shadowFg".to_string())
    .unwrap();
  theme
    .set_variable("historyUnreadBarFg".to_string(), [0x53, 0x8b, 0xb4, 0xff])
    .unwrap();
  theme
    .set_variable(
      "historyForwardChooseBg".to_string(),
      [0x00, 0x00, 0x00, 0x4c],
    )
    .unwrap();
  theme
    .link_variable(
      "historyForwardChooseFg".to_string(),
      "windowFgActive".to_string(),
    )
    .unwrap();
  theme
    .set_variable("historyPeer1NameFg".to_string(), [0xc0, 0x3d, 0x33, 0xff])
    .unwrap();
  theme
    .set_variable("historyPeer1UserpicBg".to_string(), [0xe1, 0x70, 0x76, 0xff])
    .unwrap();
  theme
    .set_variable("historyPeer2NameFg".to_string(), [0x4f, 0xad, 0x2d, 0xff])
    .unwrap();
  theme
    .set_variable("historyPeer2UserpicBg".to_string(), [0x7b, 0xc8, 0x62, 0xff])
    .unwrap();
  theme
    .set_variable("historyPeer3NameFg".to_string(), [0xd0, 0x93, 0x06, 0xff])
    .unwrap();
  theme
    .set_variable("historyPeer3UserpicBg".to_string(), [0xe5, 0xca, 0x77, 0xff])
    .unwrap();
  theme
    .link_variable(
      "historyPeer4NameFg".to_string(),
      "windowActiveTextFg".to_string(),
    )
    .unwrap();
  theme
    .set_variable("historyPeer4UserpicBg".to_string(), [0x65, 0xaa, 0xdd, 0xff])
    .unwrap();
  theme
    .set_variable("historyPeer5NameFg".to_string(), [0x85, 0x44, 0xd6, 0xff])
    .unwrap();
  theme
    .set_variable("historyPeer5UserpicBg".to_string(), [0xa6, 0x95, 0xe7, 0xff])
    .unwrap();
  theme
    .set_variable("historyPeer6NameFg".to_string(), [0xcd, 0x40, 0x73, 0xff])
    .unwrap();
  theme
    .set_variable("historyPeer6UserpicBg".to_string(), [0xee, 0x7a, 0xae, 0xff])
    .unwrap();
  theme
    .set_variable("historyPeer7NameFg".to_string(), [0x29, 0x96, 0xad, 0xff])
    .unwrap();
  theme
    .set_variable("historyPeer7UserpicBg".to_string(), [0x6e, 0xc9, 0xcb, 0xff])
    .unwrap();
  theme
    .set_variable("historyPeer8NameFg".to_string(), [0xce, 0x67, 0x1b, 0xff])
    .unwrap();
  theme
    .set_variable("historyPeer8UserpicBg".to_string(), [0xfa, 0xa7, 0x74, 0xff])
    .unwrap();
  theme
    .link_variable(
      "historyPeerUserpicFg".to_string(),
      "windowFgActive".to_string(),
    )
    .unwrap();
  theme
    .set_variable("historyScrollBarBg".to_string(), [0x00, 0x00, 0x00, 0x40])
    .unwrap();
  theme
    .set_variable(
      "historyScrollBarBgOver".to_string(),
      [0x00, 0x00, 0x00, 0x53],
    )
    .unwrap();
  theme
    .set_variable("historyScrollBg".to_string(), [0x00, 0x00, 0x00, 0x00])
    .unwrap();
  theme
    .set_variable("historyScrollBgOver".to_string(), [0x00, 0x00, 0x00, 0x1a])
    .unwrap();
  theme.link_variable("msgInBg".to_string(), "windowBg".to_string()).unwrap();
  theme
    .set_variable("msgInBgSelected".to_string(), [0xbb, 0xe1, 0xfc, 0xff])
    .unwrap();
  theme.set_variable("msgOutBg".to_string(), [0xde, 0xf1, 0xfd, 0xff]).unwrap();
  theme
    .set_variable("msgOutBgSelected".to_string(), [0xbb, 0xe1, 0xfc, 0xff])
    .unwrap();
  theme
    .set_variable("msgSelectOverlay".to_string(), [0x35, 0x8c, 0xd4, 0x4c])
    .unwrap();
  theme
    .set_variable("msgStickerOverlay".to_string(), [0x35, 0x8c, 0xd4, 0x7f])
    .unwrap();
  theme
    .link_variable(
      "msgInServiceFg".to_string(),
      "windowActiveTextFg".to_string(),
    )
    .unwrap();
  theme
    .link_variable(
      "msgInServiceFgSelected".to_string(),
      "windowActiveTextFg".to_string(),
    )
    .unwrap();
  theme
    .link_variable(
      "msgOutServiceFg".to_string(),
      "windowActiveTextFg".to_string(),
    )
    .unwrap();
  theme
    .link_variable(
      "msgOutServiceFgSelected".to_string(),
      "windowActiveTextFg".to_string(),
    )
    .unwrap();
  theme
    .set_variable("msgInShadow".to_string(), [0x74, 0x8e, 0xa2, 0x29])
    .unwrap();
  theme
    .set_variable("msgInShadowSelected".to_string(), [0x1d, 0x62, 0x97, 0x30])
    .unwrap();
  theme
    .set_variable("msgOutShadow".to_string(), [0x0d, 0x5a, 0x91, 0x1a])
    .unwrap();
  theme
    .set_variable("msgOutShadowSelected".to_string(), [0x2e, 0x74, 0xaa, 0x29])
    .unwrap();
  theme
    .set_variable("msgInDateFg".to_string(), [0xa0, 0xac, 0xb6, 0xff])
    .unwrap();
  theme
    .set_variable("msgInDateFgSelected".to_string(), [0x6a, 0x9c, 0xc5, 0xff])
    .unwrap();
  theme
    .set_variable("msgOutDateFg".to_string(), [0x86, 0xa8, 0xc2, 0xff])
    .unwrap();
  theme
    .set_variable("msgOutDateFgSelected".to_string(), [0x6c, 0xa0, 0xc2, 0xff])
    .unwrap();
  theme
    .link_variable("msgServiceFg".to_string(), "windowFgActive".to_string())
    .unwrap();
  theme
    .set_variable("msgServiceBg".to_string(), [0x00, 0x51, 0x80, 0x59])
    .unwrap();
  theme
    .set_variable("msgServiceBgSelected".to_string(), [0x62, 0xaf, 0xdd, 0xa2])
    .unwrap();
  theme
    .link_variable("msgInReplyBarColor".to_string(), "activeLineFg".to_string())
    .unwrap();
  theme
    .link_variable(
      "msgInReplyBarSelColor".to_string(),
      "activeLineFg".to_string(),
    )
    .unwrap();
  theme
    .link_variable(
      "msgOutReplyBarColor".to_string(),
      "historyOutIconFg".to_string(),
    )
    .unwrap();
  theme
    .link_variable(
      "msgOutReplyBarSelColor".to_string(),
      "historyOutIconFgSelected".to_string(),
    )
    .unwrap();
  theme
    .link_variable(
      "msgImgReplyBarColor".to_string(),
      "msgServiceFg".to_string(),
    )
    .unwrap();
  theme
    .set_variable("msgInMonoFg".to_string(), [0x4e, 0x73, 0x91, 0xff])
    .unwrap();
  theme
    .set_variable("msgOutMonoFg".to_string(), [0x4e, 0x73, 0x91, 0xff])
    .unwrap();
  theme
    .link_variable("msgDateImgFg".to_string(), "msgServiceFg".to_string())
    .unwrap();
  theme
    .set_variable("msgDateImgBg".to_string(), [0x00, 0x00, 0x00, 0x54])
    .unwrap();
  theme
    .set_variable("msgDateImgBgOver".to_string(), [0x00, 0x00, 0x00, 0x74])
    .unwrap();
  theme
    .set_variable("msgDateImgBgSelected".to_string(), [0x1c, 0x4a, 0x71, 0x87])
    .unwrap();
  theme
    .set_variable("msgFileThumbLinkInFg".to_string(), [0x16, 0x8a, 0xcd, 0xff])
    .unwrap();
  theme
    .link_variable(
      "msgFileThumbLinkInFgSelected".to_string(),
      "lightButtonFgOver".to_string(),
    )
    .unwrap();
  theme
    .set_variable("msgFileThumbLinkOutFg".to_string(), [0x0a, 0x8b, 0xd0, 0xff])
    .unwrap();
  theme
    .link_variable(
      "msgFileThumbLinkOutFgSelected".to_string(),
      "lightButtonFgOver".to_string(),
    )
    .unwrap();
  theme
    .link_variable("msgFileInBg".to_string(), "windowBgActive".to_string())
    .unwrap();
  theme
    .set_variable("msgFileInBgOver".to_string(), [0x4e, 0xad, 0xe3, 0xff])
    .unwrap();
  theme
    .set_variable("msgFileInBgSelected".to_string(), [0x51, 0xa3, 0xd3, 0xff])
    .unwrap();
  theme
    .link_variable("msgFileOutBg".to_string(), "windowBgActive".to_string())
    .unwrap();
  theme
    .set_variable("msgFileOutBgOver".to_string(), [0x4e, 0xad, 0xe3, 0xff])
    .unwrap();
  theme
    .set_variable("msgFileOutBgSelected".to_string(), [0x51, 0xa3, 0xd3, 0xff])
    .unwrap();
  theme
    .set_variable("msgFile1Bg".to_string(), [0x72, 0xb1, 0xdf, 0xff])
    .unwrap();
  theme
    .set_variable("msgFile1BgDark".to_string(), [0x5c, 0x9e, 0xce, 0xff])
    .unwrap();
  theme
    .set_variable("msgFile1BgOver".to_string(), [0x52, 0x94, 0xc4, 0xff])
    .unwrap();
  theme
    .set_variable("msgFile1BgSelected".to_string(), [0x50, 0x99, 0xd0, 0xff])
    .unwrap();
  theme
    .set_variable("msgFile2Bg".to_string(), [0x61, 0xb9, 0x6e, 0xff])
    .unwrap();
  theme
    .set_variable("msgFile2BgDark".to_string(), [0x4d, 0xa8, 0x59, 0xff])
    .unwrap();
  theme
    .set_variable("msgFile2BgOver".to_string(), [0x44, 0xa0, 0x50, 0xff])
    .unwrap();
  theme
    .set_variable("msgFile2BgSelected".to_string(), [0x46, 0xa0, 0x7e, 0xff])
    .unwrap();
  theme
    .set_variable("msgFile3Bg".to_string(), [0xe4, 0x72, 0x72, 0xff])
    .unwrap();
  theme
    .set_variable("msgFile3BgDark".to_string(), [0xcd, 0x5b, 0x5e, 0xff])
    .unwrap();
  theme
    .set_variable("msgFile3BgOver".to_string(), [0xc3, 0x51, 0x54, 0xff])
    .unwrap();
  theme
    .set_variable("msgFile3BgSelected".to_string(), [0x9f, 0x6a, 0x82, 0xff])
    .unwrap();
  theme
    .set_variable("msgFile4Bg".to_string(), [0xef, 0xc2, 0x74, 0xff])
    .unwrap();
  theme
    .set_variable("msgFile4BgDark".to_string(), [0xe6, 0xa5, 0x61, 0xff])
    .unwrap();
  theme
    .set_variable("msgFile4BgOver".to_string(), [0xdc, 0x9c, 0x5a, 0xff])
    .unwrap();
  theme
    .set_variable("msgFile4BgSelected".to_string(), [0xb1, 0x9d, 0x84, 0xff])
    .unwrap();
  theme
    .link_variable(
      "msgWaveformInActive".to_string(),
      "windowBgActive".to_string(),
    )
    .unwrap();
  theme
    .set_variable(
      "msgWaveformInActiveSelected".to_string(),
      [0x51, 0xa3, 0xd3, 0xff],
    )
    .unwrap();
  theme
    .set_variable("msgWaveformInInactive".to_string(), [0xd4, 0xde, 0xe6, 0xff])
    .unwrap();
  theme
    .set_variable(
      "msgWaveformInInactiveSelected".to_string(),
      [0x9c, 0xc1, 0xe1, 0xff],
    )
    .unwrap();
  theme
    .link_variable(
      "msgWaveformOutActive".to_string(),
      "windowBgActive".to_string(),
    )
    .unwrap();
  theme
    .set_variable(
      "msgWaveformOutActiveSelected".to_string(),
      [0x51, 0xa3, 0xd3, 0xff],
    )
    .unwrap();
  theme
    .set_variable(
      "msgWaveformOutInactive".to_string(),
      [0xb3, 0xd4, 0xe7, 0xff],
    )
    .unwrap();
  theme
    .set_variable(
      "msgWaveformOutInactiveSelected".to_string(),
      [0x87, 0xbc, 0xdb, 0xff],
    )
    .unwrap();
  theme
    .set_variable("msgBotKbOverBgAdd".to_string(), [0xff, 0xff, 0xff, 0x20])
    .unwrap();
  theme
    .link_variable("msgBotKbIconFg".to_string(), "msgServiceFg".to_string())
    .unwrap();
  theme
    .set_variable("msgBotKbRippleBg".to_string(), [0x00, 0x00, 0x00, 0x20])
    .unwrap();
  theme
    .link_variable("mediaInFg".to_string(), "msgInDateFg".to_string())
    .unwrap();
  theme
    .link_variable(
      "mediaInFgSelected".to_string(),
      "msgInDateFgSelected".to_string(),
    )
    .unwrap();
  theme
    .set_variable("mediaOutFg".to_string(), [0x80, 0xa3, 0xbd, 0xff])
    .unwrap();
  theme
    .set_variable("mediaOutFgSelected".to_string(), [0x5f, 0x8f, 0xb3, 0xff])
    .unwrap();
  theme
    .set_variable("youtubePlayIconBg".to_string(), [0xe8, 0x31, 0x31, 0xc8])
    .unwrap();
  theme
    .link_variable(
      "youtubePlayIconFg".to_string(),
      "windowFgActive".to_string(),
    )
    .unwrap();
  theme
    .set_variable("videoPlayIconBg".to_string(), [0x00, 0x00, 0x00, 0x7f])
    .unwrap();
  theme
    .set_variable("videoPlayIconFg".to_string(), [0xff, 0xff, 0xff, 0xff])
    .unwrap();
  theme.set_variable("toastBg".to_string(), [0x00, 0x00, 0x00, 0xb2]).unwrap();
  theme
    .link_variable("toastFg".to_string(), "windowFgActive".to_string())
    .unwrap();
  theme
    .link_variable("reportSpamBg".to_string(), "emojiPanHeaderBg".to_string())
    .unwrap();
  theme
    .link_variable("reportSpamFg".to_string(), "windowFg".to_string())
    .unwrap();
  theme
    .set_variable("historyToDownShadow".to_string(), [0x00, 0x00, 0x00, 0x40])
    .unwrap();
  theme
    .link_variable("historyComposeAreaBg".to_string(), "msgInBg".to_string())
    .unwrap();
  theme
    .link_variable(
      "historyComposeAreaFg".to_string(),
      "historyTextInFg".to_string(),
    )
    .unwrap();
  theme
    .link_variable(
      "historyComposeAreaFgService".to_string(),
      "msgInDateFg".to_string(),
    )
    .unwrap();
  theme
    .link_variable("historyComposeIconFg".to_string(), "menuIconFg".to_string())
    .unwrap();
  theme
    .link_variable(
      "historyComposeIconFgOver".to_string(),
      "menuIconFgOver".to_string(),
    )
    .unwrap();
  theme
    .link_variable(
      "historySendIconFg".to_string(),
      "windowBgActive".to_string(),
    )
    .unwrap();
  theme
    .link_variable(
      "historySendIconFgOver".to_string(),
      "windowBgActive".to_string(),
    )
    .unwrap();
  theme
    .link_variable(
      "historyPinnedBg".to_string(),
      "historyComposeAreaBg".to_string(),
    )
    .unwrap();
  theme
    .link_variable(
      "historyReplyBg".to_string(),
      "historyComposeAreaBg".to_string(),
    )
    .unwrap();
  theme
    .link_variable(
      "historyReplyCancelFg".to_string(),
      "cancelIconFg".to_string(),
    )
    .unwrap();
  theme
    .link_variable(
      "historyReplyCancelFgOver".to_string(),
      "cancelIconFgOver".to_string(),
    )
    .unwrap();
  theme
    .link_variable(
      "historyComposeButtonBg".to_string(),
      "historyComposeAreaBg".to_string(),
    )
    .unwrap();
  theme
    .link_variable(
      "historyComposeButtonBgOver".to_string(),
      "windowBgOver".to_string(),
    )
    .unwrap();
  theme
    .link_variable(
      "historyComposeButtonBgRipple".to_string(),
      "windowBgRipple".to_string(),
    )
    .unwrap();
  theme
    .set_variable("overviewCheckBg".to_string(), [0x00, 0x00, 0x00, 0x40])
    .unwrap();
  theme
    .link_variable("overviewCheckFg".to_string(), "windowBg".to_string())
    .unwrap();
  theme
    .link_variable("overviewCheckFgActive".to_string(), "windowBg".to_string())
    .unwrap();
  theme
    .set_variable(
      "overviewPhotoSelectOverlay".to_string(),
      [0x40, 0xac, 0xe3, 0x33],
    )
    .unwrap();
  theme
    .set_variable("profileStatusFgOver".to_string(), [0x7c, 0x99, 0xb2, 0xff])
    .unwrap();
  theme
    .link_variable(
      "notificationsBoxMonitorFg".to_string(),
      "windowFg".to_string(),
    )
    .unwrap();
  theme
    .link_variable(
      "notificationsBoxScreenBg".to_string(),
      "dialogsBgActive".to_string(),
    )
    .unwrap();
  theme
    .link_variable(
      "notificationSampleUserpicFg".to_string(),
      "windowBgActive".to_string(),
    )
    .unwrap();
  theme
    .set_variable(
      "notificationSampleCloseFg".to_string(),
      [0xd7, 0xd7, 0xd7, 0xff],
    )
    .unwrap();
  theme
    .set_variable(
      "notificationSampleTextFg".to_string(),
      [0xd7, 0xd7, 0xd7, 0xff],
    )
    .unwrap();
  theme
    .set_variable(
      "notificationSampleNameFg".to_string(),
      [0x93, 0x93, 0x93, 0xff],
    )
    .unwrap();
  theme
    .link_variable("mainMenuBg".to_string(), "windowBg".to_string())
    .unwrap();
  theme
    .link_variable("mainMenuCoverBg".to_string(), "dialogsBgActive".to_string())
    .unwrap();
  theme
    .link_variable("mainMenuCoverFg".to_string(), "windowFgActive".to_string())
    .unwrap();
  theme
    .link_variable("mediaPlayerBg".to_string(), "windowBg".to_string())
    .unwrap();
  theme
    .link_variable(
      "mediaPlayerActiveFg".to_string(),
      "windowBgActive".to_string(),
    )
    .unwrap();
  theme
    .link_variable(
      "mediaPlayerInactiveFg".to_string(),
      "sliderBgInactive".to_string(),
    )
    .unwrap();
  theme
    .set_variable("mediaPlayerDisabledFg".to_string(), [0x9d, 0xd1, 0xef, 0xff])
    .unwrap();
  theme
    .link_variable("mediaviewFileBg".to_string(), "windowBg".to_string())
    .unwrap();
  theme
    .link_variable("mediaviewFileNameFg".to_string(), "windowFg".to_string())
    .unwrap();
  theme
    .link_variable(
      "mediaviewFileSizeFg".to_string(),
      "windowSubTextFg".to_string(),
    )
    .unwrap();
  theme
    .set_variable(
      "mediaviewFileRedCornerFg".to_string(),
      [0xd5, 0x59, 0x59, 0xff],
    )
    .unwrap();
  theme
    .set_variable(
      "mediaviewFileYellowCornerFg".to_string(),
      [0xe8, 0xa6, 0x59, 0xff],
    )
    .unwrap();
  theme
    .set_variable(
      "mediaviewFileGreenCornerFg".to_string(),
      [0x49, 0xa9, 0x57, 0xff],
    )
    .unwrap();
  theme
    .set_variable(
      "mediaviewFileBlueCornerFg".to_string(),
      [0x59, 0x9d, 0xcf, 0xff],
    )
    .unwrap();
  theme
    .link_variable(
      "mediaviewFileExtFg".to_string(),
      "activeButtonFg".to_string(),
    )
    .unwrap();
  theme
    .set_variable("mediaviewMenuBg".to_string(), [0x38, 0x38, 0x38, 0xff])
    .unwrap();
  theme
    .set_variable("mediaviewMenuBgOver".to_string(), [0x50, 0x50, 0x50, 0xff])
    .unwrap();
  theme
    .set_variable("mediaviewMenuBgRipple".to_string(), [0x67, 0x67, 0x67, 0xff])
    .unwrap();
  theme
    .link_variable("mediaviewMenuFg".to_string(), "windowFgActive".to_string())
    .unwrap();
  theme
    .set_variable("mediaviewBg".to_string(), [0x22, 0x22, 0x22, 0xeb])
    .unwrap();
  theme
    .link_variable("mediaviewVideoBg".to_string(), "imageBg".to_string())
    .unwrap();
  theme
    .set_variable("mediaviewControlBg".to_string(), [0x00, 0x00, 0x00, 0x3c])
    .unwrap();
  theme
    .link_variable(
      "mediaviewControlFg".to_string(),
      "windowFgActive".to_string(),
    )
    .unwrap();
  theme
    .set_variable("mediaviewCaptionBg".to_string(), [0x11, 0x11, 0x11, 0x80])
    .unwrap();
  theme
    .link_variable(
      "mediaviewCaptionFg".to_string(),
      "mediaviewControlFg".to_string(),
    )
    .unwrap();
  theme
    .set_variable("mediaviewTextLinkFg".to_string(), [0x91, 0xd9, 0xff, 0xff])
    .unwrap();
  theme
    .link_variable("mediaviewSaveMsgBg".to_string(), "toastBg".to_string())
    .unwrap();
  theme
    .link_variable("mediaviewSaveMsgFg".to_string(), "toastFg".to_string())
    .unwrap();
  theme
    .set_variable(
      "mediaviewPlaybackActive".to_string(),
      [0xc7, 0xc7, 0xc7, 0xff],
    )
    .unwrap();
  theme
    .set_variable(
      "mediaviewPlaybackInactive".to_string(),
      [0x25, 0x25, 0x25, 0xff],
    )
    .unwrap();
  theme
    .set_variable(
      "mediaviewPlaybackActiveOver".to_string(),
      [0xff, 0xff, 0xff, 0xff],
    )
    .unwrap();
  theme
    .set_variable(
      "mediaviewPlaybackInactiveOver".to_string(),
      [0x47, 0x47, 0x47, 0xff],
    )
    .unwrap();
  theme
    .set_variable(
      "mediaviewPlaybackProgressFg".to_string(),
      [0xff, 0xff, 0xff, 0xc7],
    )
    .unwrap();
  theme
    .link_variable(
      "mediaviewPlaybackIconFg".to_string(),
      "mediaviewPlaybackActive".to_string(),
    )
    .unwrap();
  theme
    .link_variable(
      "mediaviewPlaybackIconFgOver".to_string(),
      "mediaviewPlaybackActiveOver".to_string(),
    )
    .unwrap();
  theme
    .set_variable(
      "mediaviewTransparentBg".to_string(),
      [0xff, 0xff, 0xff, 0xff],
    )
    .unwrap();
  theme
    .set_variable(
      "mediaviewTransparentFg".to_string(),
      [0xcc, 0xcc, 0xcc, 0xff],
    )
    .unwrap();
  theme
    .link_variable("notificationBg".to_string(), "windowBg".to_string())
    .unwrap();

  theme
}
