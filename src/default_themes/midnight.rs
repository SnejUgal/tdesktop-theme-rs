use super::*;

/// Generates the default Midnight theme theme.
#[must_use]
pub fn midnight() -> TdesktopTheme {
  let mut theme = TdesktopTheme::with_capacity(445);

  theme.set_variable("windowBg".to_string(), [0x17, 0x21, 0x2b, 0xff]).unwrap();
  theme.set_variable("windowFg".to_string(), [0xf5, 0xf5, 0xf5, 0xff]).unwrap();
  theme
    .set_variable("windowBgOver".to_string(), [0x23, 0x2e, 0x3c, 0xff])
    .unwrap();
  theme
    .set_variable("windowBgRipple".to_string(), [0x24, 0x30, 0x3d, 0xff])
    .unwrap();
  theme
    .set_variable("windowFgOver".to_string(), [0xe9, 0xec, 0xf0, 0xff])
    .unwrap();
  theme
    .set_variable("windowSubTextFg".to_string(), [0x70, 0x84, 0x99, 0xff])
    .unwrap();
  theme
    .set_variable("windowSubTextFgOver".to_string(), [0x7c, 0x90, 0xa4, 0xff])
    .unwrap();
  theme
    .set_variable("windowBoldFg".to_string(), [0xe9, 0xe8, 0xe8, 0xff])
    .unwrap();
  theme
    .set_variable("windowBoldFgOver".to_string(), [0xe9, 0xe9, 0xe9, 0xff])
    .unwrap();
  theme
    .set_variable("windowBgActive".to_string(), [0x52, 0x88, 0xc1, 0xff])
    .unwrap();
  theme
    .set_variable("windowFgActive".to_string(), [0xff, 0xff, 0xff, 0xff])
    .unwrap();
  theme
    .set_variable("windowActiveTextFg".to_string(), [0x6a, 0xb3, 0xf3, 0xff])
    .unwrap();
  theme
    .set_variable("windowShadowFg".to_string(), [0x00, 0x00, 0x00, 0xff])
    .unwrap();
  theme
    .set_variable(
      "windowShadowFgFallback".to_string(),
      [0x17, 0x21, 0x2b, 0xff],
    )
    .unwrap();
  theme.set_variable("shadowFg".to_string(), [0x04, 0x08, 0x0e, 0x56]).unwrap();
  theme
    .set_variable("slideFadeOutBg".to_string(), [0x00, 0x00, 0x00, 0x3c])
    .unwrap();
  theme
    .link_variable(
      "slideFadeOutShadowFg".to_string(),
      "windowShadowFg".to_string(),
    )
    .unwrap();
  theme.set_variable("imageBg".to_string(), [0x00, 0x00, 0x00, 0xff]).unwrap();
  theme
    .set_variable("imageBgTransparent".to_string(), [0xff, 0xff, 0xff, 0xff])
    .unwrap();
  theme
    .set_variable("activeButtonBg".to_string(), [0x2f, 0x6e, 0xa5, 0xff])
    .unwrap();
  theme
    .set_variable("activeButtonBgOver".to_string(), [0x34, 0x76, 0xab, 0xff])
    .unwrap();
  theme
    .set_variable("activeButtonBgRipple".to_string(), [0x3b, 0x7c, 0xb1, 0xff])
    .unwrap();
  theme
    .set_variable("activeButtonFg".to_string(), [0xff, 0xff, 0xff, 0xff])
    .unwrap();
  theme
    .set_variable("activeButtonFgOver".to_string(), [0xff, 0xff, 0xff, 0xff])
    .unwrap();
  theme
    .set_variable(
      "activeButtonSecondaryFg".to_string(),
      [0x9a, 0xbf, 0xe7, 0xff],
    )
    .unwrap();
  theme
    .link_variable(
      "activeButtonSecondaryFgOver".to_string(),
      "activeButtonSecondaryFg".to_string(),
    )
    .unwrap();
  theme
    .set_variable("activeLineFg".to_string(), [0x63, 0x96, 0xcb, 0xff])
    .unwrap();
  theme
    .set_variable("activeLineFgError".to_string(), [0xef, 0x59, 0x59, 0xff])
    .unwrap();
  theme
    .set_variable("lightButtonBg".to_string(), [0x17, 0x21, 0x2b, 0xff])
    .unwrap();
  theme
    .set_variable("lightButtonBgOver".to_string(), [0x1d, 0x2a, 0x39, 0xff])
    .unwrap();
  theme
    .set_variable("lightButtonBgRipple".to_string(), [0x22, 0x31, 0x43, 0xff])
    .unwrap();
  theme
    .set_variable("lightButtonFg".to_string(), [0x6a, 0xb2, 0xf2, 0xff])
    .unwrap();
  theme
    .link_variable("lightButtonFgOver".to_string(), "lightButtonFg".to_string())
    .unwrap();
  theme
    .set_variable("attentionButtonFg".to_string(), [0xec, 0x39, 0x42, 0xff])
    .unwrap();
  theme
    .set_variable("attentionButtonFgOver".to_string(), [0xec, 0x39, 0x42, 0xff])
    .unwrap();
  theme
    .set_variable("attentionButtonBgOver".to_string(), [0x59, 0x2a, 0x2a, 0x64])
    .unwrap();
  theme
    .set_variable(
      "attentionButtonBgRipple".to_string(),
      [0x68, 0x32, 0x32, 0x64],
    )
    .unwrap();
  theme
    .link_variable("outlineButtonBg".to_string(), "windowBg".to_string())
    .unwrap();
  theme
    .set_variable("outlineButtonBgOver".to_string(), [0x20, 0x2b, 0x37, 0xff])
    .unwrap();
  theme
    .set_variable(
      "outlineButtonOutlineFg".to_string(),
      [0x39, 0x83, 0xc3, 0xff],
    )
    .unwrap();
  theme
    .set_variable("outlineButtonBgRipple".to_string(), [0x27, 0x32, 0x3f, 0xff])
    .unwrap();
  theme.set_variable("menuBg".to_string(), [0x17, 0x21, 0x2b, 0xff]).unwrap();
  theme
    .set_variable("menuBgOver".to_string(), [0xff, 0xff, 0xff, 0xff])
    .unwrap();
  theme
    .set_variable("menuBgRipple".to_string(), [0x1f, 0x29, 0x34, 0xff])
    .unwrap();
  theme
    .set_variable("menuIconFg".to_string(), [0x6c, 0x78, 0x83, 0xff])
    .unwrap();
  theme
    .set_variable("menuIconFgOver".to_string(), [0xdc, 0xdc, 0xdc, 0xff])
    .unwrap();
  theme
    .set_variable("menuSubmenuArrowFg".to_string(), [0x63, 0x7c, 0x95, 0xff])
    .unwrap();
  theme
    .set_variable("menuFgDisabled".to_string(), [0x3d, 0x4e, 0x5c, 0xff])
    .unwrap();
  theme
    .set_variable("menuSeparatorFg".to_string(), [0x23, 0x2f, 0x39, 0xff])
    .unwrap();
  theme
    .set_variable("scrollBarBg".to_string(), [0xff, 0xff, 0xff, 0x53])
    .unwrap();
  theme
    .set_variable("scrollBarBgOver".to_string(), [0xff, 0xff, 0xff, 0x7a])
    .unwrap();
  theme.set_variable("scrollBg".to_string(), [0xff, 0xff, 0xff, 0x1a]).unwrap();
  theme
    .set_variable("scrollBgOver".to_string(), [0xff, 0xff, 0xff, 0x2c])
    .unwrap();
  theme
    .set_variable("smallCloseIconFg".to_string(), [0x55, 0x61, 0x6c, 0xff])
    .unwrap();
  theme
    .set_variable("smallCloseIconFgOver".to_string(), [0xa3, 0xa3, 0xa3, 0xff])
    .unwrap();
  theme
    .link_variable("radialFg".to_string(), "windowFgActive".to_string())
    .unwrap();
  theme.set_variable("radialBg".to_string(), [0x00, 0x00, 0x00, 0x56]).unwrap();
  theme
    .set_variable("placeholderFg".to_string(), [0x6d, 0x78, 0x83, 0xff])
    .unwrap();
  theme
    .set_variable("placeholderFgActive".to_string(), [0x4f, 0x5b, 0x67, 0xff])
    .unwrap();
  theme
    .set_variable("inputBorderFg".to_string(), [0x31, 0x3c, 0x49, 0xff])
    .unwrap();
  theme
    .set_variable("filterInputBorderFg".to_string(), [0x24, 0x2f, 0x3d, 0xff])
    .unwrap();
  theme
    .set_variable("filterInputInactiveBg".to_string(), [0x24, 0x2f, 0x3d, 0xff])
    .unwrap();
  theme
    .set_variable("checkboxFg".to_string(), [0x4f, 0x62, 0x76, 0xff])
    .unwrap();
  theme
    .set_variable("sliderBgInactive".to_string(), [0x36, 0x45, 0x54, 0xff])
    .unwrap();
  theme
    .set_variable("sliderBgActive".to_string(), [0x4a, 0x81, 0xbc, 0xff])
    .unwrap();
  theme
    .set_variable("tooltipBg".to_string(), [0x16, 0x22, 0x2d, 0xff])
    .unwrap();
  theme
    .set_variable("tooltipFg".to_string(), [0xd4, 0xe0, 0xea, 0xff])
    .unwrap();
  theme
    .set_variable("tooltipBorderFg".to_string(), [0x16, 0x22, 0x2d, 0xff])
    .unwrap();
  theme
    .set_variable("titleShadow".to_string(), [0x00, 0x00, 0x00, 0x00])
    .unwrap();
  theme.set_variable("titleBg".to_string(), [0x1f, 0x29, 0x36, 0xff]).unwrap();
  theme
    .set_variable("titleBgActive".to_string(), [0x24, 0x2f, 0x3d, 0xff])
    .unwrap();
  theme
    .link_variable("titleButtonBg".to_string(), "titleBg".to_string())
    .unwrap();
  theme
    .set_variable("titleButtonFg".to_string(), [0x57, 0x66, 0x73, 0xff])
    .unwrap();
  theme
    .set_variable("titleButtonBgOver".to_string(), [0x25, 0x30, 0x3e, 0xff])
    .unwrap();
  theme
    .set_variable("titleButtonFgOver".to_string(), [0xe0, 0xe0, 0xe0, 0xff])
    .unwrap();
  theme
    .set_variable("titleButtonBgActive".to_string(), [0x24, 0x2f, 0x3d, 0xff])
    .unwrap();
  theme
    .link_variable(
      "titleButtonFgActive".to_string(),
      "titleButtonFg".to_string(),
    )
    .unwrap();
  theme
    .set_variable(
      "titleButtonBgActiveOver".to_string(),
      [0x2c, 0x38, 0x47, 0xff],
    )
    .unwrap();
  theme
    .link_variable(
      "titleButtonFgActiveOver".to_string(),
      "titleButtonFgOver".to_string(),
    )
    .unwrap();
  theme
    .link_variable(
      "titleButtonCloseBg".to_string(),
      "titleButtonBg".to_string(),
    )
    .unwrap();
  theme
    .link_variable(
      "titleButtonCloseFg".to_string(),
      "titleButtonFg".to_string(),
    )
    .unwrap();
  theme
    .set_variable(
      "titleButtonCloseBgOver".to_string(),
      [0xe9, 0x25, 0x39, 0xff],
    )
    .unwrap();
  theme
    .link_variable(
      "titleButtonCloseFgOver".to_string(),
      "windowFgActive".to_string(),
    )
    .unwrap();
  theme
    .set_variable(
      "titleButtonCloseBgActive".to_string(),
      [0x24, 0x2f, 0x3d, 0xff],
    )
    .unwrap();
  theme
    .link_variable(
      "titleButtonCloseFgActive".to_string(),
      "titleButtonCloseFg".to_string(),
    )
    .unwrap();
  theme
    .set_variable(
      "titleButtonCloseBgActiveOver".to_string(),
      [0xe9, 0x25, 0x39, 0xff],
    )
    .unwrap();
  theme
    .link_variable(
      "titleButtonCloseFgActiveOver".to_string(),
      "titleButtonCloseFgOver".to_string(),
    )
    .unwrap();
  theme.set_variable("titleFg".to_string(), [0x6a, 0x76, 0x80, 0xff]).unwrap();
  theme
    .set_variable("titleFgActive".to_string(), [0x91, 0xa3, 0xb3, 0xff])
    .unwrap();
  theme
    .set_variable("trayCounterBg".to_string(), [0xf2, 0x3c, 0x34, 0xff])
    .unwrap();
  theme
    .set_variable("trayCounterBgMute".to_string(), [0x88, 0x88, 0x88, 0xff])
    .unwrap();
  theme
    .set_variable("trayCounterFg".to_string(), [0xff, 0xff, 0xff, 0xff])
    .unwrap();
  theme
    .set_variable(
      "trayCounterBgMacInvert".to_string(),
      [0xff, 0xff, 0xff, 0xff],
    )
    .unwrap();
  theme
    .set_variable(
      "trayCounterFgMacInvert".to_string(),
      [0xff, 0xff, 0xff, 0x01],
    )
    .unwrap();
  theme.set_variable("layerBg".to_string(), [0x00, 0x00, 0x00, 0x7f]).unwrap();
  theme
    .set_variable("cancelIconFg".to_string(), [0x73, 0x7e, 0x87, 0xff])
    .unwrap();
  theme
    .set_variable("cancelIconFgOver".to_string(), [0xdc, 0xdc, 0xdc, 0xff])
    .unwrap();
  theme.set_variable("boxBg".to_string(), [0x17, 0x21, 0x2b, 0xff]).unwrap();
  theme.link_variable("boxTextFg".to_string(), "windowFg".to_string()).unwrap();
  theme
    .set_variable("boxTextFgGood".to_string(), [0x55, 0x98, 0xdb, 0xff])
    .unwrap();
  theme
    .set_variable("boxTextFgError".to_string(), [0xdc, 0x3d, 0x3d, 0xff])
    .unwrap();
  theme
    .set_variable("boxTitleFg".to_string(), [0xeb, 0xeb, 0xeb, 0xff])
    .unwrap();
  theme
    .set_variable("boxSearchBg".to_string(), [0x17, 0x21, 0x2b, 0xff])
    .unwrap();
  theme
    .set_variable("boxTitleAdditionalFg".to_string(), [0x6d, 0x7f, 0x8f, 0xff])
    .unwrap();
  theme
    .link_variable("boxTitleCloseFg".to_string(), "cancelIconFg".to_string())
    .unwrap();
  theme
    .link_variable(
      "boxTitleCloseFgOver".to_string(),
      "cancelIconFgOver".to_string(),
    )
    .unwrap();
  theme
    .set_variable("membersAboutLimitFg".to_string(), [0x67, 0x7a, 0x8b, 0xff])
    .unwrap();
  theme
    .set_variable("contactsBg".to_string(), [0x17, 0x21, 0x2b, 0xff])
    .unwrap();
  theme
    .set_variable("contactsBgOver".to_string(), [0x1f, 0x2c, 0x39, 0xff])
    .unwrap();
  theme
    .link_variable("contactsNameFg".to_string(), "boxTextFg".to_string())
    .unwrap();
  theme
    .set_variable("contactsStatusFg".to_string(), [0x67, 0x7a, 0x8b, 0xff])
    .unwrap();
  theme
    .set_variable("contactsStatusFgOver".to_string(), [0x67, 0x7a, 0x8b, 0xff])
    .unwrap();
  theme
    .set_variable(
      "contactsStatusFgOnline".to_string(),
      [0x55, 0x9b, 0xe0, 0xff],
    )
    .unwrap();
  theme
    .link_variable("photoCropFadeBg".to_string(), "layerBg".to_string())
    .unwrap();
  theme
    .set_variable("photoCropPointFg".to_string(), [0xff, 0xff, 0xff, 0x7f])
    .unwrap();
  theme
    .set_variable("callArrowFg".to_string(), [0x49, 0xa2, 0xf0, 0xff])
    .unwrap();
  theme
    .set_variable("callArrowMissedFg".to_string(), [0xed, 0x50, 0x50, 0xff])
    .unwrap();
  theme.link_variable("introBg".to_string(), "windowBg".to_string()).unwrap();
  theme
    .set_variable("introTitleFg".to_string(), [0xee, 0xee, 0xee, 0xff])
    .unwrap();
  theme
    .set_variable("introDescriptionFg".to_string(), [0x72, 0x87, 0x9b, 0xff])
    .unwrap();
  theme
    .set_variable("introErrorFg".to_string(), [0xe9, 0x40, 0x40, 0xff])
    .unwrap();
  theme
    .set_variable("introCoverTopBg".to_string(), [0x12, 0x4a, 0x82, 0xff])
    .unwrap();
  theme
    .set_variable("introCoverBottomBg".to_string(), [0x23, 0x65, 0x9f, 0xff])
    .unwrap();
  theme
    .set_variable("introCoverIconsFg".to_string(), [0x3b, 0x7c, 0xbd, 0xff])
    .unwrap();
  theme
    .set_variable("introCoverPlaneTrace".to_string(), [0x38, 0x70, 0xa8, 0xff])
    .unwrap();
  theme
    .set_variable("introCoverPlaneInner".to_string(), [0xce, 0xd9, 0xe2, 0xff])
    .unwrap();
  theme
    .set_variable("introCoverPlaneOuter".to_string(), [0x97, 0xa9, 0xb5, 0xff])
    .unwrap();
  theme
    .set_variable("introCoverPlaneTop".to_string(), [0xff, 0xff, 0xff, 0xff])
    .unwrap();
  theme
    .link_variable("dialogsMenuIconFg".to_string(), "menuIconFg".to_string())
    .unwrap();
  theme
    .link_variable(
      "dialogsMenuIconFgOver".to_string(),
      "menuIconFgOver".to_string(),
    )
    .unwrap();
  theme.link_variable("dialogsBg".to_string(), "windowBg".to_string()).unwrap();
  theme
    .set_variable("dialogsNameFg".to_string(), [0xf5, 0xf5, 0xf5, 0xff])
    .unwrap();
  theme
    .link_variable("dialogsChatIconFg".to_string(), "dialogsNameFg".to_string())
    .unwrap();
  theme
    .set_variable("dialogsDateFg".to_string(), [0x86, 0x96, 0xa8, 0xff])
    .unwrap();
  theme
    .set_variable("dialogsTextFg".to_string(), [0x7f, 0x91, 0xa4, 0xff])
    .unwrap();
  theme
    .set_variable("dialogsTextFgService".to_string(), [0x73, 0xb9, 0xf5, 0xff])
    .unwrap();
  theme
    .set_variable("dialogsDraftFg".to_string(), [0xff, 0x52, 0x5d, 0xff])
    .unwrap();
  theme
    .set_variable("dialogsVerifiedIconBg".to_string(), [0x6a, 0xb3, 0xf3, 0xff])
    .unwrap();
  theme
    .set_variable("dialogsVerifiedIconFg".to_string(), [0x17, 0x21, 0x2b, 0xff])
    .unwrap();
  theme
    .set_variable("dialogsSendingIconFg".to_string(), [0x64, 0x7d, 0x96, 0xff])
    .unwrap();
  theme
    .set_variable("dialogsSentIconFg".to_string(), [0x72, 0xbc, 0xfd, 0xff])
    .unwrap();
  theme
    .set_variable("dialogsUnreadBg".to_string(), [0x40, 0x82, 0xbc, 0xff])
    .unwrap();
  theme
    .set_variable("dialogsUnreadBgMuted".to_string(), [0x3e, 0x54, 0x6a, 0xff])
    .unwrap();
  theme
    .set_variable("dialogsUnreadFg".to_string(), [0xff, 0xff, 0xff, 0xff])
    .unwrap();
  theme
    .set_variable("dialogsBgOver".to_string(), [0x20, 0x2b, 0x36, 0xff])
    .unwrap();
  theme
    .link_variable(
      "dialogsNameFgOver".to_string(),
      "windowBoldFgOver".to_string(),
    )
    .unwrap();
  theme
    .link_variable(
      "dialogsChatIconFgOver".to_string(),
      "dialogsNameFgOver".to_string(),
    )
    .unwrap();
  theme
    .set_variable("dialogsDateFgOver".to_string(), [0x84, 0x95, 0xa9, 0xff])
    .unwrap();
  theme
    .set_variable("dialogsTextFgOver".to_string(), [0x91, 0xa3, 0xb5, 0xff])
    .unwrap();
  theme
    .set_variable(
      "dialogsTextFgServiceOver".to_string(),
      [0x73, 0xb9, 0xf5, 0xff],
    )
    .unwrap();
  theme
    .link_variable(
      "dialogsDraftFgOver".to_string(),
      "dialogsDraftFg".to_string(),
    )
    .unwrap();
  theme
    .set_variable(
      "dialogsVerifiedIconBgOver".to_string(),
      [0x6a, 0xb3, 0xf3, 0xff],
    )
    .unwrap();
  theme
    .link_variable(
      "dialogsVerifiedIconFgOver".to_string(),
      "dialogsVerifiedIconFg".to_string(),
    )
    .unwrap();
  theme
    .link_variable(
      "dialogsSendingIconFgOver".to_string(),
      "dialogsSendingIconFg".to_string(),
    )
    .unwrap();
  theme
    .set_variable("dialogsSentIconFgOver".to_string(), [0x72, 0xbc, 0xfd, 0xff])
    .unwrap();
  theme
    .set_variable("dialogsUnreadBgOver".to_string(), [0x40, 0x82, 0xbc, 0xff])
    .unwrap();
  theme
    .set_variable(
      "dialogsUnreadBgMutedOver".to_string(),
      [0x4d, 0x57, 0x62, 0xff],
    )
    .unwrap();
  theme
    .link_variable(
      "dialogsUnreadFgOver".to_string(),
      "dialogsUnreadFg".to_string(),
    )
    .unwrap();
  theme
    .set_variable("dialogsBgActive".to_string(), [0x2b, 0x52, 0x78, 0xff])
    .unwrap();
  theme
    .link_variable(
      "dialogsNameFgActive".to_string(),
      "windowFgActive".to_string(),
    )
    .unwrap();
  theme
    .link_variable(
      "dialogsChatIconFgActive".to_string(),
      "dialogsNameFgActive".to_string(),
    )
    .unwrap();
  theme
    .link_variable(
      "dialogsDateFgActive".to_string(),
      "windowFgActive".to_string(),
    )
    .unwrap();
  theme
    .link_variable(
      "dialogsTextFgActive".to_string(),
      "windowFgActive".to_string(),
    )
    .unwrap();
  theme
    .link_variable(
      "dialogsTextFgServiceActive".to_string(),
      "dialogsTextFgActive".to_string(),
    )
    .unwrap();
  theme
    .set_variable("dialogsDraftFgActive".to_string(), [0xa9, 0xca, 0xf0, 0xff])
    .unwrap();
  theme
    .link_variable(
      "dialogsVerifiedIconBgActive".to_string(),
      "dialogsTextFgActive".to_string(),
    )
    .unwrap();
  theme
    .link_variable(
      "dialogsVerifiedIconFgActive".to_string(),
      "dialogsBgActive".to_string(),
    )
    .unwrap();
  theme
    .set_variable(
      "dialogsSendingIconFgActive".to_string(),
      [0xff, 0xff, 0xff, 0x99],
    )
    .unwrap();
  theme
    .link_variable(
      "dialogsSentIconFgActive".to_string(),
      "dialogsTextFgActive".to_string(),
    )
    .unwrap();
  theme
    .link_variable(
      "dialogsUnreadBgActive".to_string(),
      "dialogsTextFgActive".to_string(),
    )
    .unwrap();
  theme
    .set_variable(
      "dialogsUnreadBgMutedActive".to_string(),
      [0x7a, 0xa3, 0xca, 0xff],
    )
    .unwrap();
  theme
    .set_variable("dialogsUnreadFgActive".to_string(), [0x2a, 0x53, 0x78, 0xff])
    .unwrap();
  theme
    .set_variable("dialogsRippleBg".to_string(), [0x25, 0x31, 0x3d, 0xff])
    .unwrap();
  theme
    .set_variable("dialogsRippleBgActive".to_string(), [0x31, 0x5a, 0x80, 0xff])
    .unwrap();
  theme
    .link_variable(
      "dialogsForwardBg".to_string(),
      "dialogsBgActive".to_string(),
    )
    .unwrap();
  theme
    .link_variable(
      "dialogsForwardFg".to_string(),
      "dialogsNameFgActive".to_string(),
    )
    .unwrap();
  theme
    .set_variable("searchedBarBg".to_string(), [0x20, 0x2b, 0x38, 0xff])
    .unwrap();
  theme
    .set_variable("searchedBarFg".to_string(), [0x8c, 0x98, 0xa4, 0xff])
    .unwrap();
  theme.set_variable("topBarBg".to_string(), [0x17, 0x21, 0x2b, 0xff]).unwrap();
  theme
    .link_variable("emojiPanBg".to_string(), "windowBg".to_string())
    .unwrap();
  theme
    .set_variable("emojiPanCategories".to_string(), [0x17, 0x21, 0x2b, 0xff])
    .unwrap();
  theme
    .set_variable("emojiPanHeaderFg".to_string(), [0x7c, 0x8d, 0x9e, 0xff])
    .unwrap();
  theme
    .set_variable("emojiPanHeaderBg".to_string(), [0xff, 0xff, 0xff, 0xf2])
    .unwrap();
  theme
    .set_variable("stickerPanDeleteBg".to_string(), [0x00, 0x00, 0x00, 0xcc])
    .unwrap();
  theme
    .link_variable(
      "stickerPanDeleteFg".to_string(),
      "windowFgActive".to_string(),
    )
    .unwrap();
  theme
    .set_variable("stickerPreviewBg".to_string(), [0x00, 0x00, 0x00, 0xb0])
    .unwrap();
  theme
    .link_variable("historyTextInFg".to_string(), "windowFg".to_string())
    .unwrap();
  theme
    .set_variable(
      "historyTextInFgSelected".to_string(),
      [0xff, 0xff, 0xff, 0xff],
    )
    .unwrap();
  theme
    .set_variable("historyTextOutFg".to_string(), [0xe4, 0xec, 0xf2, 0xff])
    .unwrap();
  theme
    .set_variable(
      "historyTextOutFgSelected".to_string(),
      [0xff, 0xff, 0xff, 0xff],
    )
    .unwrap();
  theme
    .set_variable("historyLinkInFg".to_string(), [0x70, 0xba, 0xf5, 0xff])
    .unwrap();
  theme
    .set_variable(
      "historyLinkInFgSelected".to_string(),
      [0xaa, 0xdc, 0xff, 0xff],
    )
    .unwrap();
  theme
    .set_variable("historyLinkOutFg".to_string(), [0x83, 0xca, 0xff, 0xff])
    .unwrap();
  theme
    .set_variable(
      "historyLinkOutFgSelected".to_string(),
      [0xaa, 0xdc, 0xff, 0xff],
    )
    .unwrap();
  theme
    .link_variable(
      "historyFileNameInFg".to_string(),
      "historyTextInFg".to_string(),
    )
    .unwrap();
  theme
    .set_variable(
      "historyFileNameInFgSelected".to_string(),
      [0xff, 0xff, 0xff, 0xff],
    )
    .unwrap();
  theme
    .link_variable(
      "historyFileNameOutFg".to_string(),
      "historyTextOutFg".to_string(),
    )
    .unwrap();
  theme
    .set_variable(
      "historyFileNameOutFgSelected".to_string(),
      [0xff, 0xff, 0xff, 0xff],
    )
    .unwrap();
  theme
    .set_variable("historyOutIconFg".to_string(), [0x6b, 0xbf, 0xff, 0xff])
    .unwrap();
  theme
    .set_variable(
      "historyOutIconFgSelected".to_string(),
      [0xff, 0xff, 0xff, 0xff],
    )
    .unwrap();
  theme
    .set_variable("historyIconFgInverted".to_string(), [0xff, 0xff, 0xff, 0xe5])
    .unwrap();
  theme
    .set_variable(
      "historySendingOutIconFg".to_string(),
      [0x70, 0xa4, 0xd2, 0xff],
    )
    .unwrap();
  theme
    .set_variable(
      "historySendingInIconFg".to_string(),
      [0x76, 0x83, 0x8b, 0xff],
    )
    .unwrap();
  theme
    .set_variable(
      "historySendingInvertedIconFg".to_string(),
      [0xff, 0xff, 0xff, 0xc8],
    )
    .unwrap();
  theme
    .set_variable("historyCallArrowInFg".to_string(), [0x50, 0x93, 0xd6, 0xff])
    .unwrap();
  theme
    .set_variable(
      "historyCallArrowInFgSelected".to_string(),
      [0xff, 0xff, 0xff, 0xff],
    )
    .unwrap();
  theme
    .link_variable(
      "historyCallArrowMissedInFg".to_string(),
      "callArrowMissedFg".to_string(),
    )
    .unwrap();
  theme
    .set_variable(
      "historyCallArrowMissedInFgSelected".to_string(),
      [0xff, 0xff, 0xff, 0xff],
    )
    .unwrap();
  theme
    .set_variable("historyCallArrowOutFg".to_string(), [0xff, 0xff, 0xff, 0xff])
    .unwrap();
  theme
    .set_variable(
      "historyCallArrowOutFgSelected".to_string(),
      [0xff, 0xff, 0xff, 0xff],
    )
    .unwrap();
  theme
    .set_variable("historyUnreadBarBg".to_string(), [0x18, 0x24, 0x33, 0xff])
    .unwrap();
  theme
    .set_variable(
      "historyUnreadBarBorder".to_string(),
      [0x03, 0x08, 0x0d, 0x00],
    )
    .unwrap();
  theme
    .set_variable("historyUnreadBarFg".to_string(), [0xff, 0xff, 0xff, 0xff])
    .unwrap();
  theme
    .set_variable(
      "historyForwardChooseBg".to_string(),
      [0x00, 0x00, 0x00, 0x4c],
    )
    .unwrap();
  theme
    .link_variable(
      "historyForwardChooseFg".to_string(),
      "windowFgActive".to_string(),
    )
    .unwrap();
  theme
    .set_variable("historyPeer1NameFg".to_string(), [0xfb, 0x61, 0x69, 0xff])
    .unwrap();
  theme
    .set_variable(
      "historyPeer1NameFgSelected".to_string(),
      [0xff, 0xff, 0xff, 0xff],
    )
    .unwrap();
  theme
    .set_variable("historyPeer1UserpicBg".to_string(), [0xdd, 0x45, 0x54, 0xff])
    .unwrap();
  theme
    .set_variable("historyPeer2NameFg".to_string(), [0x85, 0xde, 0x85, 0xff])
    .unwrap();
  theme
    .set_variable(
      "historyPeer2NameFgSelected".to_string(),
      [0xff, 0xff, 0xff, 0xff],
    )
    .unwrap();
  theme
    .set_variable("historyPeer2UserpicBg".to_string(), [0x63, 0xaa, 0x55, 0xff])
    .unwrap();
  theme
    .set_variable("historyPeer3NameFg".to_string(), [0xf3, 0xbc, 0x5c, 0xff])
    .unwrap();
  theme
    .set_variable(
      "historyPeer3NameFgSelected".to_string(),
      [0xff, 0xff, 0xff, 0xff],
    )
    .unwrap();
  theme
    .set_variable("historyPeer3UserpicBg".to_string(), [0xd0, 0x97, 0x3c, 0xff])
    .unwrap();
  theme
    .set_variable("historyPeer4NameFg".to_string(), [0x65, 0xbd, 0xf3, 0xff])
    .unwrap();
  theme
    .set_variable(
      "historyPeer4NameFgSelected".to_string(),
      [0xff, 0xff, 0xff, 0xff],
    )
    .unwrap();
  theme
    .set_variable("historyPeer4UserpicBg".to_string(), [0x43, 0x88, 0xb9, 0xff])
    .unwrap();
  theme
    .set_variable("historyPeer5NameFg".to_string(), [0xb4, 0x8b, 0xf2, 0xff])
    .unwrap();
  theme
    .set_variable(
      "historyPeer5NameFgSelected".to_string(),
      [0xff, 0xff, 0xff, 0xff],
    )
    .unwrap();
  theme
    .set_variable("historyPeer5UserpicBg".to_string(), [0x79, 0x65, 0xc1, 0xff])
    .unwrap();
  theme
    .set_variable("historyPeer6NameFg".to_string(), [0xff, 0x56, 0x94, 0xff])
    .unwrap();
  theme
    .set_variable(
      "historyPeer6NameFgSelected".to_string(),
      [0xff, 0xff, 0xff, 0xff],
    )
    .unwrap();
  theme
    .set_variable("historyPeer6UserpicBg".to_string(), [0xcb, 0x4f, 0x87, 0xff])
    .unwrap();
  theme
    .set_variable("historyPeer7NameFg".to_string(), [0x62, 0xd4, 0xe3, 0xff])
    .unwrap();
  theme
    .set_variable(
      "historyPeer7NameFgSelected".to_string(),
      [0xff, 0xff, 0xff, 0xff],
    )
    .unwrap();
  theme
    .set_variable("historyPeer7UserpicBg".to_string(), [0x41, 0xa4, 0xa6, 0xff])
    .unwrap();
  theme
    .set_variable("historyPeer8NameFg".to_string(), [0xfa, 0xa3, 0x57, 0xff])
    .unwrap();
  theme
    .set_variable(
      "historyPeer8NameFgSelected".to_string(),
      [0xff, 0xff, 0xff, 0xff],
    )
    .unwrap();
  theme
    .set_variable("historyPeer8UserpicBg".to_string(), [0xdb, 0x86, 0x3b, 0xff])
    .unwrap();
  theme
    .link_variable(
      "historyPeerUserpicFg".to_string(),
      "windowFgActive".to_string(),
    )
    .unwrap();
  theme
    .set_variable("historyScrollBarBg".to_string(), [0x7f, 0x84, 0x89, 0x7a])
    .unwrap();
  theme
    .set_variable(
      "historyScrollBarBgOver".to_string(),
      [0x64, 0x68, 0x6c, 0xbc],
    )
    .unwrap();
  theme
    .set_variable("historyScrollBg".to_string(), [0x56, 0x5a, 0x5e, 0x4c])
    .unwrap();
  theme
    .set_variable("historyScrollBgOver".to_string(), [0x5a, 0x5d, 0x61, 0x6b])
    .unwrap();
  theme.set_variable("msgInBg".to_string(), [0x18, 0x25, 0x33, 0xff]).unwrap();
  theme
    .set_variable("msgInBgSelected".to_string(), [0x2e, 0x70, 0xa5, 0xff])
    .unwrap();
  theme.set_variable("msgOutBg".to_string(), [0x2b, 0x52, 0x78, 0xff]).unwrap();
  theme
    .set_variable("msgOutBgSelected".to_string(), [0x2e, 0x70, 0xa5, 0xff])
    .unwrap();
  theme
    .set_variable("msgSelectOverlay".to_string(), [0x35, 0x85, 0xd4, 0x4c])
    .unwrap();
  theme
    .set_variable("msgStickerOverlay".to_string(), [0x35, 0x85, 0xd4, 0x7f])
    .unwrap();
  theme
    .set_variable("msgInServiceFg".to_string(), [0x71, 0xba, 0xfa, 0xff])
    .unwrap();
  theme
    .set_variable(
      "msgInServiceFgSelected".to_string(),
      [0xff, 0xff, 0xff, 0xff],
    )
    .unwrap();
  theme
    .set_variable("msgOutServiceFg".to_string(), [0x90, 0xca, 0xff, 0xff])
    .unwrap();
  theme
    .set_variable(
      "msgOutServiceFgSelected".to_string(),
      [0xff, 0xff, 0xff, 0xff],
    )
    .unwrap();
  theme
    .set_variable("msgInShadow".to_string(), [0x74, 0x8e, 0xa2, 0x00])
    .unwrap();
  theme
    .set_variable("msgInShadowSelected".to_string(), [0x53, 0x8e, 0xbb, 0x00])
    .unwrap();
  theme
    .set_variable("msgOutShadow".to_string(), [0x00, 0x00, 0x00, 0x00])
    .unwrap();
  theme
    .set_variable("msgOutShadowSelected".to_string(), [0x36, 0x6e, 0xa6, 0x00])
    .unwrap();
  theme
    .set_variable("msgInDateFg".to_string(), [0x6d, 0x7f, 0x8f, 0xff])
    .unwrap();
  theme
    .set_variable("msgInDateFgSelected".to_string(), [0xff, 0xff, 0xff, 0xff])
    .unwrap();
  theme
    .set_variable("msgOutDateFg".to_string(), [0x7d, 0xa8, 0xd3, 0xff])
    .unwrap();
  theme
    .set_variable("msgOutDateFgSelected".to_string(), [0xff, 0xff, 0xff, 0xff])
    .unwrap();
  theme
    .link_variable("msgServiceFg".to_string(), "windowFgActive".to_string())
    .unwrap();
  theme
    .set_variable("msgServiceBg".to_string(), [0x21, 0x30, 0x40, 0xd5])
    .unwrap();
  theme
    .set_variable("msgServiceBgSelected".to_string(), [0x2e, 0x7a, 0xb4, 0xff])
    .unwrap();
  theme
    .set_variable("msgInReplyBarColor".to_string(), [0x42, 0x9b, 0xdb, 0xff])
    .unwrap();
  theme
    .set_variable("msgInReplyBarSelColor".to_string(), [0xff, 0xff, 0xff, 0xff])
    .unwrap();
  theme
    .set_variable("msgOutReplyBarColor".to_string(), [0x65, 0xb9, 0xf4, 0xff])
    .unwrap();
  theme
    .set_variable(
      "msgOutReplyBarSelColor".to_string(),
      [0xff, 0xff, 0xff, 0xff],
    )
    .unwrap();
  theme
    .link_variable(
      "msgImgReplyBarColor".to_string(),
      "msgServiceFg".to_string(),
    )
    .unwrap();
  theme
    .set_variable("msgInMonoFg".to_string(), [0x5a, 0x8c, 0xb7, 0xff])
    .unwrap();
  theme
    .set_variable("msgOutMonoFg".to_string(), [0xae, 0xd1, 0xf3, 0xff])
    .unwrap();
  theme
    .set_variable("msgInMonoFgSelected".to_string(), [0xa3, 0xcd, 0xf7, 0xff])
    .unwrap();
  theme
    .set_variable("msgOutMonoFgSelected".to_string(), [0xa3, 0xcd, 0xf7, 0xff])
    .unwrap();
  theme
    .set_variable("msgDateImgFg".to_string(), [0xff, 0xff, 0xff, 0xf2])
    .unwrap();
  theme
    .set_variable("msgDateImgBg".to_string(), [0x00, 0x00, 0x00, 0x54])
    .unwrap();
  theme
    .set_variable("msgDateImgBgOver".to_string(), [0x00, 0x00, 0x00, 0x74])
    .unwrap();
  theme
    .set_variable("msgDateImgBgSelected".to_string(), [0x20, 0x4f, 0x78, 0x87])
    .unwrap();
  theme
    .link_variable(
      "msgFileThumbLinkInFg".to_string(),
      "lightButtonFg".to_string(),
    )
    .unwrap();
  theme
    .set_variable(
      "msgFileThumbLinkInFgSelected".to_string(),
      [0xff, 0xff, 0xff, 0xff],
    )
    .unwrap();
  theme
    .set_variable("msgFileThumbLinkOutFg".to_string(), [0x6c, 0x9e, 0xcf, 0xff])
    .unwrap();
  theme
    .set_variable(
      "msgFileThumbLinkOutFgSelected".to_string(),
      [0xff, 0xff, 0xff, 0xff],
    )
    .unwrap();
  theme
    .set_variable("msgFileInBg".to_string(), [0x3f, 0x96, 0xd0, 0xff])
    .unwrap();
  theme
    .set_variable("msgFileInBgOver".to_string(), [0x48, 0x9e, 0xd7, 0xff])
    .unwrap();
  theme
    .set_variable("msgFileInBgSelected".to_string(), [0x6a, 0xb4, 0xf4, 0xff])
    .unwrap();
  theme
    .set_variable("msgFileOutBg".to_string(), [0x4c, 0x9c, 0xe2, 0xff])
    .unwrap();
  theme
    .set_variable("msgFileOutBgOver".to_string(), [0x54, 0xa4, 0xe9, 0xff])
    .unwrap();
  theme
    .set_variable("msgFileOutBgSelected".to_string(), [0x58, 0xab, 0xf3, 0xff])
    .unwrap();
  theme
    .set_variable("msgFile1Bg".to_string(), [0x3e, 0x7e, 0xba, 0xff])
    .unwrap();
  theme
    .set_variable("msgFile1BgDark".to_string(), [0x24, 0x67, 0x9e, 0xff])
    .unwrap();
  theme
    .set_variable("msgFile1BgOver".to_string(), [0x1d, 0x5e, 0x93, 0xff])
    .unwrap();
  theme
    .set_variable("msgFile1BgSelected".to_string(), [0xff, 0xff, 0xff, 0xff])
    .unwrap();
  theme
    .set_variable("msgFile2Bg".to_string(), [0x3e, 0xa3, 0x4a, 0xff])
    .unwrap();
  theme
    .set_variable("msgFile2BgDark".to_string(), [0x29, 0x88, 0x35, 0xff])
    .unwrap();
  theme
    .set_variable("msgFile2BgOver".to_string(), [0x1b, 0x77, 0x25, 0xff])
    .unwrap();
  theme
    .set_variable("msgFile2BgSelected".to_string(), [0x1b, 0x77, 0x25, 0xff])
    .unwrap();
  theme
    .set_variable("msgFile3Bg".to_string(), [0xd6, 0x45, 0x4c, 0xff])
    .unwrap();
  theme
    .set_variable("msgFile3BgDark".to_string(), [0xbf, 0x33, 0x3a, 0xff])
    .unwrap();
  theme
    .set_variable("msgFile3BgOver".to_string(), [0xb2, 0x28, 0x2f, 0xff])
    .unwrap();
  theme
    .set_variable("msgFile3BgSelected".to_string(), [0xb2, 0x28, 0x2f, 0xff])
    .unwrap();
  theme
    .set_variable("msgFile4Bg".to_string(), [0xd9, 0x95, 0x46, 0xff])
    .unwrap();
  theme
    .set_variable("msgFile4BgDark".to_string(), [0xc1, 0x7d, 0x39, 0xff])
    .unwrap();
  theme
    .set_variable("msgFile4BgOver".to_string(), [0xac, 0x6b, 0x29, 0xff])
    .unwrap();
  theme
    .set_variable("msgFile4BgSelected".to_string(), [0xac, 0x6b, 0x29, 0xff])
    .unwrap();
  theme
    .set_variable("historyFileInIconFg".to_string(), [0xff, 0xff, 0xff, 0xff])
    .unwrap();
  theme
    .set_variable(
      "historyFileInIconFgSelected".to_string(),
      [0xff, 0xff, 0xff, 0xff],
    )
    .unwrap();
  theme
    .set_variable("historyFileInRadialFg".to_string(), [0xff, 0xff, 0xff, 0xff])
    .unwrap();
  theme
    .link_variable(
      "historyFileInRadialFgSelected".to_string(),
      "historyFileInIconFgSelected".to_string(),
    )
    .unwrap();
  theme
    .set_variable("historyFileOutIconFg".to_string(), [0xff, 0xff, 0xff, 0xff])
    .unwrap();
  theme
    .set_variable(
      "historyFileOutIconFgSelected".to_string(),
      [0xff, 0xff, 0xff, 0xff],
    )
    .unwrap();
  theme
    .set_variable(
      "historyFileOutRadialFg".to_string(),
      [0xff, 0xff, 0xff, 0xff],
    )
    .unwrap();
  theme
    .set_variable(
      "historyFileOutRadialFgSelected".to_string(),
      [0xff, 0xff, 0xff, 0xff],
    )
    .unwrap();
  theme
    .set_variable(
      "historyFileThumbIconFg".to_string(),
      [0xef, 0xef, 0xef, 0xff],
    )
    .unwrap();
  theme
    .set_variable(
      "historyFileThumbIconFgSelected".to_string(),
      [0xff, 0xff, 0xff, 0xff],
    )
    .unwrap();
  theme
    .link_variable(
      "historyFileThumbRadialFg".to_string(),
      "historyFileThumbIconFg".to_string(),
    )
    .unwrap();
  theme
    .set_variable(
      "historyFileThumbRadialFgSelected".to_string(),
      [0xff, 0xff, 0xff, 0xff],
    )
    .unwrap();
  theme
    .link_variable(
      "historyVideoMessageProgressFg".to_string(),
      "historyFileThumbIconFg".to_string(),
    )
    .unwrap();
  theme
    .set_variable("msgWaveformInActive".to_string(), [0x54, 0x9c, 0xd7, 0xff])
    .unwrap();
  theme
    .set_variable(
      "msgWaveformInActiveSelected".to_string(),
      [0xff, 0xff, 0xff, 0xff],
    )
    .unwrap();
  theme
    .set_variable("msgWaveformInInactive".to_string(), [0x3a, 0x4d, 0x61, 0xff])
    .unwrap();
  theme
    .set_variable(
      "msgWaveformInInactiveSelected".to_string(),
      [0x6f, 0xa5, 0xd4, 0xff],
    )
    .unwrap();
  theme
    .set_variable("msgWaveformOutActive".to_string(), [0x62, 0xb2, 0xfd, 0xff])
    .unwrap();
  theme
    .set_variable(
      "msgWaveformOutActiveSelected".to_string(),
      [0xff, 0xff, 0xff, 0xff],
    )
    .unwrap();
  theme
    .set_variable(
      "msgWaveformOutInactive".to_string(),
      [0x4b, 0x7f, 0xb3, 0xff],
    )
    .unwrap();
  theme
    .set_variable(
      "msgWaveformOutInactiveSelected".to_string(),
      [0x6f, 0xa5, 0xd4, 0xff],
    )
    .unwrap();
  theme
    .set_variable("msgBotKbOverBgAdd".to_string(), [0x80, 0xb1, 0xdb, 0x0f])
    .unwrap();
  theme
    .link_variable("msgBotKbIconFg".to_string(), "msgServiceFg".to_string())
    .unwrap();
  theme
    .set_variable("msgBotKbRippleBg".to_string(), [0x92, 0xc0, 0xe5, 0x0b])
    .unwrap();
  theme
    .link_variable("mediaInFg".to_string(), "msgInDateFg".to_string())
    .unwrap();
  theme
    .link_variable(
      "mediaInFgSelected".to_string(),
      "msgInDateFgSelected".to_string(),
    )
    .unwrap();
  theme
    .link_variable("mediaOutFg".to_string(), "msgOutDateFg".to_string())
    .unwrap();
  theme
    .link_variable(
      "mediaOutFgSelected".to_string(),
      "msgOutDateFgSelected".to_string(),
    )
    .unwrap();
  theme
    .set_variable("youtubePlayIconBg".to_string(), [0xe8, 0x31, 0x31, 0xc8])
    .unwrap();
  theme
    .link_variable(
      "youtubePlayIconFg".to_string(),
      "windowFgActive".to_string(),
    )
    .unwrap();
  theme
    .set_variable("videoPlayIconBg".to_string(), [0x00, 0x00, 0x00, 0x7f])
    .unwrap();
  theme
    .set_variable("videoPlayIconFg".to_string(), [0xff, 0xff, 0xff, 0xff])
    .unwrap();
  theme.set_variable("toastBg".to_string(), [0x00, 0x00, 0x00, 0xb2]).unwrap();
  theme
    .link_variable("toastFg".to_string(), "windowFgActive".to_string())
    .unwrap();
  theme
    .set_variable("reportSpamBg".to_string(), [0x36, 0x3c, 0x42, 0xff])
    .unwrap();
  theme
    .link_variable("reportSpamFg".to_string(), "windowFg".to_string())
    .unwrap();
  theme
    .set_variable("historyToDownBg".to_string(), [0x1d, 0x2b, 0x3a, 0xff])
    .unwrap();
  theme
    .set_variable("historyToDownBgOver".to_string(), [0x24, 0x34, 0x46, 0xff])
    .unwrap();
  theme
    .set_variable("historyToDownBgRipple".to_string(), [0x2b, 0x3c, 0x4f, 0xff])
    .unwrap();
  theme
    .set_variable("historyToDownFg".to_string(), [0xad, 0xb4, 0xba, 0xff])
    .unwrap();
  theme
    .link_variable(
      "historyToDownFgOver".to_string(),
      "menuIconFgOver".to_string(),
    )
    .unwrap();
  theme
    .set_variable("historyToDownShadow".to_string(), [0x00, 0x00, 0x00, 0x40])
    .unwrap();
  theme
    .set_variable("historyComposeAreaBg".to_string(), [0x17, 0x21, 0x2b, 0xff])
    .unwrap();
  theme
    .link_variable(
      "historyComposeAreaFg".to_string(),
      "historyTextInFg".to_string(),
    )
    .unwrap();
  theme
    .link_variable(
      "historyComposeAreaFgService".to_string(),
      "msgInDateFg".to_string(),
    )
    .unwrap();
  theme
    .link_variable("historyComposeIconFg".to_string(), "menuIconFg".to_string())
    .unwrap();
  theme
    .link_variable(
      "historyComposeIconFgOver".to_string(),
      "menuIconFgOver".to_string(),
    )
    .unwrap();
  theme
    .link_variable(
      "historySendIconFg".to_string(),
      "windowBgActive".to_string(),
    )
    .unwrap();
  theme
    .link_variable(
      "historySendIconFgOver".to_string(),
      "windowBgActive".to_string(),
    )
    .unwrap();
  theme
    .set_variable("historyPinnedBg".to_string(), [0x1b, 0x27, 0x34, 0xff])
    .unwrap();
  theme
    .link_variable(
      "historyReplyBg".to_string(),
      "historyComposeAreaBg".to_string(),
    )
    .unwrap();
  theme
    .link_variable(
      "historyReplyIconFg".to_string(),
      "windowBgActive".to_string(),
    )
    .unwrap();
  theme
    .link_variable(
      "historyReplyCancelFg".to_string(),
      "cancelIconFg".to_string(),
    )
    .unwrap();
  theme
    .link_variable(
      "historyReplyCancelFgOver".to_string(),
      "cancelIconFgOver".to_string(),
    )
    .unwrap();
  theme
    .link_variable(
      "historyComposeButtonBg".to_string(),
      "historyComposeAreaBg".to_string(),
    )
    .unwrap();
  theme
    .set_variable(
      "historyComposeButtonBgOver".to_string(),
      [0x1c, 0x28, 0x35, 0xff],
    )
    .unwrap();
  theme
    .set_variable(
      "historyComposeButtonBgRipple".to_string(),
      [0x22, 0x2f, 0x3e, 0xff],
    )
    .unwrap();
  theme
    .set_variable("overviewCheckBg".to_string(), [0x00, 0x00, 0x00, 0x40])
    .unwrap();
  theme
    .set_variable("overviewCheckFg".to_string(), [0xff, 0xff, 0xff, 0xff])
    .unwrap();
  theme
    .set_variable("overviewCheckFgActive".to_string(), [0xff, 0xff, 0xff, 0xff])
    .unwrap();
  theme
    .set_variable(
      "overviewPhotoSelectOverlay".to_string(),
      [0x3f, 0xa2, 0xe3, 0x33],
    )
    .unwrap();
  theme
    .set_variable("profileStatusFgOver".to_string(), [0x67, 0x7a, 0x8b, 0xff])
    .unwrap();
  theme
    .link_variable(
      "profileVerifiedCheckBg".to_string(),
      "windowBgActive".to_string(),
    )
    .unwrap();
  theme
    .link_variable(
      "profileVerifiedCheckFg".to_string(),
      "windowFgActive".to_string(),
    )
    .unwrap();
  theme
    .set_variable("profileAdminStartFg".to_string(), [0x62, 0xa9, 0xe6, 0xff])
    .unwrap();
  theme
    .link_variable(
      "notificationsBoxMonitorFg".to_string(),
      "windowFg".to_string(),
    )
    .unwrap();
  theme
    .link_variable(
      "notificationsBoxScreenBg".to_string(),
      "dialogsBgActive".to_string(),
    )
    .unwrap();
  theme
    .link_variable(
      "notificationSampleUserpicFg".to_string(),
      "windowBgActive".to_string(),
    )
    .unwrap();
  theme
    .set_variable(
      "notificationSampleCloseFg".to_string(),
      [0xd7, 0xd7, 0xd7, 0xff],
    )
    .unwrap();
  theme
    .set_variable(
      "notificationSampleTextFg".to_string(),
      [0xd7, 0xd7, 0xd7, 0xff],
    )
    .unwrap();
  theme
    .set_variable(
      "notificationSampleNameFg".to_string(),
      [0x93, 0x93, 0x93, 0xff],
    )
    .unwrap();
  theme
    .link_variable(
      "changePhoneSimcardFrom".to_string(),
      "notificationSampleTextFg".to_string(),
    )
    .unwrap();
  theme
    .link_variable(
      "changePhoneSimcardTo".to_string(),
      "notificationSampleNameFg".to_string(),
    )
    .unwrap();
  theme
    .link_variable("mainMenuBg".to_string(), "windowBg".to_string())
    .unwrap();
  theme
    .set_variable("mainMenuCoverBg".to_string(), [0x27, 0x68, 0x99, 0xff])
    .unwrap();
  theme
    .link_variable("mainMenuCoverFg".to_string(), "windowFgActive".to_string())
    .unwrap();
  theme
    .link_variable("mainMenuCloudFg".to_string(), "activeButtonFg".to_string())
    .unwrap();
  theme
    .set_variable("mainMenuCloudBg".to_string(), [0x18, 0x56, 0x80, 0xff])
    .unwrap();
  theme
    .link_variable("mediaPlayerBg".to_string(), "windowBg".to_string())
    .unwrap();
  theme
    .link_variable(
      "mediaPlayerActiveFg".to_string(),
      "windowBgActive".to_string(),
    )
    .unwrap();
  theme
    .link_variable(
      "mediaPlayerInactiveFg".to_string(),
      "sliderBgInactive".to_string(),
    )
    .unwrap();
  theme
    .set_variable("mediaPlayerDisabledFg".to_string(), [0x9d, 0xd1, 0xef, 0xff])
    .unwrap();
  theme
    .link_variable("mediaviewFileBg".to_string(), "windowBg".to_string())
    .unwrap();
  theme
    .link_variable("mediaviewFileNameFg".to_string(), "windowFg".to_string())
    .unwrap();
  theme
    .link_variable(
      "mediaviewFileSizeFg".to_string(),
      "windowSubTextFg".to_string(),
    )
    .unwrap();
  theme
    .set_variable(
      "mediaviewFileRedCornerFg".to_string(),
      [0xd5, 0x59, 0x59, 0xff],
    )
    .unwrap();
  theme
    .set_variable(
      "mediaviewFileYellowCornerFg".to_string(),
      [0xe8, 0xa6, 0x59, 0xff],
    )
    .unwrap();
  theme
    .set_variable(
      "mediaviewFileGreenCornerFg".to_string(),
      [0x49, 0xa9, 0x57, 0xff],
    )
    .unwrap();
  theme
    .set_variable(
      "mediaviewFileBlueCornerFg".to_string(),
      [0x59, 0x9d, 0xcf, 0xff],
    )
    .unwrap();
  theme
    .link_variable(
      "mediaviewFileExtFg".to_string(),
      "activeButtonFg".to_string(),
    )
    .unwrap();
  theme
    .set_variable("mediaviewMenuBg".to_string(), [0x38, 0x38, 0x38, 0xff])
    .unwrap();
  theme
    .set_variable("mediaviewMenuBgOver".to_string(), [0x50, 0x50, 0x50, 0xff])
    .unwrap();
  theme
    .set_variable("mediaviewMenuBgRipple".to_string(), [0x67, 0x67, 0x67, 0xff])
    .unwrap();
  theme
    .link_variable("mediaviewMenuFg".to_string(), "windowFgActive".to_string())
    .unwrap();
  theme
    .set_variable("mediaviewBg".to_string(), [0x22, 0x22, 0x22, 0xeb])
    .unwrap();
  theme
    .link_variable("mediaviewVideoBg".to_string(), "imageBg".to_string())
    .unwrap();
  theme
    .set_variable("mediaviewControlBg".to_string(), [0x00, 0x00, 0x00, 0x3c])
    .unwrap();
  theme
    .link_variable(
      "mediaviewControlFg".to_string(),
      "windowFgActive".to_string(),
    )
    .unwrap();
  theme
    .set_variable("mediaviewCaptionBg".to_string(), [0x11, 0x11, 0x11, 0x80])
    .unwrap();
  theme
    .link_variable(
      "mediaviewCaptionFg".to_string(),
      "mediaviewControlFg".to_string(),
    )
    .unwrap();
  theme
    .set_variable("mediaviewTextLinkFg".to_string(), [0x70, 0xba, 0xf5, 0xff])
    .unwrap();
  theme
    .link_variable("mediaviewSaveMsgBg".to_string(), "toastBg".to_string())
    .unwrap();
  theme
    .link_variable("mediaviewSaveMsgFg".to_string(), "toastFg".to_string())
    .unwrap();
  theme
    .set_variable(
      "mediaviewPlaybackActive".to_string(),
      [0xc7, 0xc7, 0xc7, 0xff],
    )
    .unwrap();
  theme
    .set_variable(
      "mediaviewPlaybackInactive".to_string(),
      [0x25, 0x25, 0x25, 0xff],
    )
    .unwrap();
  theme
    .set_variable(
      "mediaviewPlaybackActiveOver".to_string(),
      [0xff, 0xff, 0xff, 0xff],
    )
    .unwrap();
  theme
    .set_variable(
      "mediaviewPlaybackInactiveOver".to_string(),
      [0x47, 0x47, 0x47, 0xff],
    )
    .unwrap();
  theme
    .set_variable(
      "mediaviewPlaybackProgressFg".to_string(),
      [0xff, 0xff, 0xff, 0xc7],
    )
    .unwrap();
  theme
    .link_variable(
      "mediaviewPlaybackIconFg".to_string(),
      "mediaviewPlaybackActive".to_string(),
    )
    .unwrap();
  theme
    .link_variable(
      "mediaviewPlaybackIconFgOver".to_string(),
      "mediaviewPlaybackActiveOver".to_string(),
    )
    .unwrap();
  theme
    .set_variable(
      "mediaviewTransparentBg".to_string(),
      [0xff, 0xff, 0xff, 0xff],
    )
    .unwrap();
  theme
    .set_variable(
      "mediaviewTransparentFg".to_string(),
      [0xcc, 0xcc, 0xcc, 0xff],
    )
    .unwrap();
  theme
    .link_variable("notificationBg".to_string(), "windowBg".to_string())
    .unwrap();
  theme.set_variable("callBg".to_string(), [0x14, 0x19, 0x1f, 0xf5]).unwrap();
  theme
    .set_variable("callNameFg".to_string(), [0xff, 0xff, 0xff, 0xff])
    .unwrap();
  theme
    .set_variable("callFingerprintBg".to_string(), [0x00, 0x00, 0x00, 0x66])
    .unwrap();
  theme
    .set_variable("callStatusFg".to_string(), [0xaa, 0xab, 0xac, 0xff])
    .unwrap();
  theme
    .set_variable("callIconFg".to_string(), [0xff, 0xff, 0xff, 0xff])
    .unwrap();
  theme
    .set_variable("callAnswerBg".to_string(), [0x48, 0x8f, 0xc9, 0xff])
    .unwrap();
  theme
    .set_variable("callAnswerRipple".to_string(), [0x42, 0x86, 0xc2, 0xff])
    .unwrap();
  theme
    .set_variable("callAnswerBgOuter".to_string(), [0x3f, 0x95, 0xeb, 0x26])
    .unwrap();
  theme
    .set_variable("callHangupBg".to_string(), [0xcc, 0x46, 0x46, 0xff])
    .unwrap();
  theme
    .set_variable("callHangupRipple".to_string(), [0xca, 0x41, 0x41, 0xff])
    .unwrap();
  theme
    .set_variable("callCancelBg".to_string(), [0xff, 0xff, 0xff, 0xff])
    .unwrap();
  theme
    .set_variable("callCancelFg".to_string(), [0x53, 0x62, 0x71, 0xff])
    .unwrap();
  theme
    .set_variable("callCancelRipple".to_string(), [0x43, 0x53, 0x63, 0xff])
    .unwrap();
  theme
    .set_variable("callMuteRipple".to_string(), [0xff, 0xff, 0xff, 0x12])
    .unwrap();
  theme
    .set_variable("callBarBg".to_string(), [0x36, 0x66, 0x93, 0xff])
    .unwrap();
  theme
    .set_variable("callBarMuteRipple".to_string(), [0x4b, 0x7d, 0xab, 0xff])
    .unwrap();
  theme
    .set_variable("callBarBgMuted".to_string(), [0x35, 0x49, 0x5d, 0xff])
    .unwrap();
  theme
    .set_variable("callBarUnmuteRipple".to_string(), [0x43, 0x5b, 0x73, 0xff])
    .unwrap();
  theme
    .link_variable("callBarFg".to_string(), "dialogsNameFgActive".to_string())
    .unwrap();
  theme
    .link_variable("importantTooltipBg".to_string(), "toastBg".to_string())
    .unwrap();
  theme
    .link_variable("importantTooltipFg".to_string(), "toastFg".to_string())
    .unwrap();
  theme
    .set_variable(
      "importantTooltipFgLink".to_string(),
      [0x65, 0xb6, 0xfc, 0xff],
    )
    .unwrap();
  theme
    .set_variable("filterInputActiveBg".to_string(), [0x24, 0x2f, 0x3d, 0xff])
    .unwrap();
  theme.set_variable("botKbBg".to_string(), [0x24, 0x2f, 0x3d, 0xff]).unwrap();
  theme
    .set_variable("botKbDownBg".to_string(), [0x2d, 0x3a, 0x49, 0xff])
    .unwrap();
  theme
    .set_variable("emojiIconFg".to_string(), [0x6c, 0x78, 0x83, 0xff])
    .unwrap();
  theme
    .set_variable("emojiIconFgActive".to_string(), [0x51, 0xa0, 0xe5, 0xff])
    .unwrap();
  theme
    .set_variable("overviewCheckBorder".to_string(), [0xe4, 0xea, 0xef, 0xff])
    .unwrap();

  theme
}
