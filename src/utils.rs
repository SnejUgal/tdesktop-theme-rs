//! Provides a few utility functions.

/// Checks that `name` is a valid variable name.
///
/// A variable name may only consist of latin letters, digits and the underscore
/// (`_`).
///
/// # Examples
///
/// ```
/// use tdesktop_theme::utils::is_variable_name_valid;
///
/// assert!(is_variable_name_valid("windowBg"));
/// assert!(is_variable_name_valid("window_bg"));
/// assert!(!is_variable_name_valid("window     Bg"));
/// assert!(!is_variable_name_valid("ываываывыδφγδσ"));
/// assert!(!is_variable_name_valid("!#@$)@"));
/// ```
pub fn is_variable_name_valid(name: &str) -> bool {
  name.chars().all(|symbol| {
    (symbol >= 'a' && symbol <= 'z')
      || (symbol >= 'A' && symbol <= 'Z')
      || (symbol >= '0' && symbol <= '9')
      || symbol == '_'
  })
}
