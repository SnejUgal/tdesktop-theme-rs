//! A crate to work with Telegram Desktop themes. Supports:
//!
//! - Parsing of `.tdesktop-palette`;
//! - Parsing of `.tdesktop-theme`;
//! - Serialization to `.tdesktop-palette` format;
//! - Serialization to `.tdesktop-theme` format;
//! - Dealing with wallpapers;
//! - Editing themes;
//! - Variables links.
//!
//! Also can:
//! - Resolve links;
//! - Unlink variables;
//! - Add themes (`&theme + &other_theme`);
//! - Fallback to another theme (`&theme | &other_theme`);
//! - Provide you with Telegram's default themes.
//!
//! We also provide the default themes under the `default_themes` module.
//! If you think we don't support something or you wish we had some feature,
//! feel free to [fill an issue on our GitLab repository][issues].
//!
//! [issues]: https://gitlab.com/SnejUgal/tdesktop-theme-rs/issues

#![deny(warnings)]
#![deny(future_incompatible)]
#![deny(nonstandard_style)]

#[cfg(test)]
use indexmap::indexmap;

pub mod default_themes;
mod parse;
mod serialize;
mod tdesktop_theme;
pub mod utils;
mod wallpaper;

pub use self::parse::ParseError;
pub use self::tdesktop_theme::*;
pub use self::wallpaper::*;
use indexmap::IndexMap;

/// An array that represents a color, in the format
/// `[red, green, blue, alpha]`.
///
/// An array was chosen because it is usable with other crates, unlike if this
/// crate used his own struct.
type Color = [u8; 4];

type Variables = IndexMap<String, Value>;

/// Represents a possible variable's value.
#[derive(Debug, PartialEq, Clone)]
pub enum Value {
  /// Holds the color of the variable.
  Color(Color),
  /// Holds the variable's link to another variable.
  ///
  /// # Notes
  ///
  /// Tt's not guaranteed that the theme really has the variable this field
  /// holds a link to.
  ///
  /// To get the variable's real value, use `TdesktopTheme.resolve_variable`.
  Link(String),
}
